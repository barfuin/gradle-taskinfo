# gradle-taskinfo

**Gradle plugin that displays task dependencies and types**

For a given task, this plugin can:

- display the task dependencies as a tree (**tiTree**),
- display the execution queue, i.e. the order in which tasks would be executed (**tiOrder**), and
- export this information as a JSON file (**tiJson**).

When any of these tasks are invoked, all other tasks are disabled. (So you can see the dependencies of a
"dropDatabase" task without actually dropping the database.)  
This plugin leverages Gradle's
[configuration avoidance](https://docs.gradle.org/8.5/userguide/task_configuration_avoidance.html), so it incurs
no burden at all on your build. This means you can just add it to all of your projects, and have its diagnostic
capabilities on hand immediately when you need them.


#### Prerequisites

- Gradle 3.4 or newer
- Java 8 or newer


## How to use

*gradle-taskinfo* is published to the
[Gradle Plugin Portal](https://plugins.gradle.org/plugin/org.barfuin.gradle.taskinfo),
so you can just use it in your build like so:

**build.gradle:**

```groovy
plugins {
    id 'org.barfuin.gradle.taskinfo' version '2.2.0'
}
```

The plugin provides three tasks:

### tiTree

The **tiTree** task displays all dependencies and finalizers of a task as a tree. For example,

```
./gradlew tiTree assemble
```

will output something like this (your individual tasks may be wired differently):

```
:assemble                             (org.gradle.api.DefaultTask)
+--- :jar                             (org.gradle.api.tasks.bundling.Jar)
|    `--- :classes                    (org.gradle.api.DefaultTask)
|         +--- :compileJava           (org.gradle.api.tasks.compile.JavaCompile)
|         `--- :processResources      (org.gradle.language.jvm.tasks.ProcessResources)
+--- :javadocJar                      (org.gradle.api.tasks.bundling.Jar)
|    `--- :javadoc                    (org.gradle.api.tasks.javadoc.Javadoc)
|         `--- :classes               (org.gradle.api.DefaultTask)
|              +--- :compileJava      (org.gradle.api.tasks.compile.JavaCompile)
|              `--- :processResources (org.gradle.language.jvm.tasks.ProcessResources)
`--- :sourcesJar                      (org.gradle.api.tasks.bundling.Jar)
```

Each task is shown with its implementation type on the right. The output is colored by default.

The tree will also include finalizer tasks, which are technically not dependencies because they are executed *after*
the given task. However, they *are* executed, and have dependencies themselves, so they are listed here for
completeness. Finalizer tasks are marked `(finalizer)`.


### tiOrder

The **tiOrder** task displays all tasks that would be executed, in their order in the execution queue. For example,

```
./gradlew tiOrder assemble
```

will output something like this (your individual tasks may be wired differently):

```
In order to execute task ':assemble', the following tasks would be executed in this order:

  1. :compileJava      (org.gradle.api.tasks.compile.JavaCompile)
  2. :processResources (org.gradle.language.jvm.tasks.ProcessResources)
  3. :classes          (org.gradle.api.DefaultTask)
  4. :jar              (org.gradle.api.tasks.bundling.Jar)
  5. :javadoc          (org.gradle.api.tasks.javadoc.Javadoc)
  6. :javadocJar       (org.gradle.api.tasks.bundling.Jar)
  7. :sourcesJar       (org.gradle.api.tasks.bundling.Jar)
  8. :assemble         (org.gradle.api.DefaultTask)
```

Each task is shown with its implementation type on the right. The output is colored by default.

Finalizer tasks are also included here as per their position in the execution queue. However, they are not marked,
because their designation as finalizers is lost after having been assigned a position in the execution queue.
(Also, a "finalizer" designation would be meaningless, because tasks may appear both as a regular dependency and as
a finalizer in the same Gradle invocation.)


### tiJson

The **tiJson** task exports a JSON document containing a hierarchical view of all the task's dependencies and
finalizers. For example,

```
./gradlew tiJson assemble
```

will output a JSON file called *build/taskinfo/taskinfo-assemble.json*, containing the information also shown by the
**tiTree** task, plus some additional data (name and group). The full JSON file would be too long for this README, but
each record looks like this:

```json-doc
{
  "name" : "compileJava",
  "path" : ":compileJava",
  "finalizer" : false,
  "group" : null,
  "type" : "org.gradle.api.tasks.compile.JavaCompile",
  "queuePosition" : 1,
  "dependencies": [
    // Other records like this one; field absent when empty.
  ]
}
```

The fields of the above record are mostly self-explanatory. The `queuePosition` is the number that would have been
shown in front of the task by **tiOrder** (starting at 1, see above). The `group` field (string) will always be present,
but if the task has not been assigned to a task group, its value will be `null`.

The **tiJson** task should be useful for unit-testing complex task wiring. Also, the resulting JSON file can be further
processed by tools such as [jq](https://stedolan.github.io/jq/) or the [TaskInfo Viewer for
VSCode](https://marketplace.visualstudio.com/items?itemName=sandipchitale.vscode-taskinfo-viewer). In general, it
should be used if you need to parse or postprocess the *gradle-taskinfo* output somehow. This task is for the
machines - the other two are for humans.


### A Note on Gradle Task Matching

When calling the tasks above, the command line suggests that the tasks to be analyzed are given as arguments to our
tasks. That is in fact not the case. Instead, they are given to Gradle for execution, but our plugin prevents their
execution and reports on the analysis instead. This behavior is required so that we get real-world results from
Gradle. (You get the most accurate results.)

This also means that Gradle performs its
[task matching](https://docs.gradle.org/8.5/userguide/command_line_interface.html#sec:command_line_executing_tasks)
on our "task arguments". For example, when calling `./gradlew tiOrder build`, you may find that the *tiOrder* tasks
outputs this message: `In order to execute [:build, :subproject1:build, :subproject2:build], the following tasks would
be executed [...]`. So, even though you seemingly specified only one task argument (`build`), it was matched to three
by Gradle, because an additional `build` task exists in the two subprojects. An invocation of just `./gradlew build`
would behave the same. So again, this is good for us because the results of our tasks are more accurate.

In order to explicitly select only one particular task, use the Gradle path notation (the one with the colons). For
example, by calling `./gradlew tiOrder :build`, only the `build` task from the root project will be used.


### Activating this plugin user-wide

It is possible to configure Gradle to add this plugin to every Gradle project of the current user.

In the directory `$GRADLE_USER_HOME`, create a file init.d/taskinfo.gradle. If the init.d subdirectory doesn't exist,
create it, too. Then copy &amp; paste this content into the file:

```groovy
initscript {
    repositories {
        gradlePluginPortal()
    }
    dependencies {
        classpath group: 'org.barfuin.gradle.taskinfo', name: 'gradle-taskinfo', version: '2.2.0'
    }
}
rootProject {
    apply plugin: org.barfuin.gradle.taskinfo.GradleTaskInfoPlugin
}
```


### Limitations

This plugin currently has the following limitations:

- We do not currently support [composite builds](https://docs.gradle.org/8.5/userguide/composite_builds.html).
  (Note that we *do* support [multi-project builds](https://docs.gradle.org/8.5/userguide/multi_project_builds.html).)  
  The reason for this limitation is the fact that (a) Gradle does not provide sufficient information about tasks in
  included builds (the data is simply not there for us to display, not even in Gradle's bowels), and (b) Gradle run
  tasks from the included builds before it runs our plugin, so we can't disable them properly.  
  Therefore, by default, we safeguard by refusing to run in a composite build. If you understand the risks from running
  tasks in included builds, you may specify `-Ptaskinfo.disableSafeguard=true` as a Gradle option to disable the
  safeguard.  
  We do our best to display what information there is about tasks from included builds, should they occur in our
  dependency tree. Your mileage may vary, mostly depending on the Gradle version.

&nbsp;

----------------------------------------------------------------------------------------------------------------------

## Configuration

*gradle-taskinfo* offers a few configuration options:

| Option            | Type    | Default | Property Name            | Description                                                                                                                                                                                                          |
|:------------------|---------|---------|:-------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **clipped**       | boolean | `false` | `taskinfo.clipped`       | `true` → In the tree view, prevent printing a subtree of the same task more than once<br>`false` → print everything                                                                                                  |
| **color**         | boolean | `true`  | `taskinfo.color`         | `true` → colored output<br>`false` → no color                                                                                                                                                                        |
| **showTaskTypes** | boolean | `true`  | `taskinfo.showTaskTypes` | `true` → Task types displayed by **tiTree** and **tiOrder** tasks<br>`false` → Task types will be omitted                                                                                                            |
| **internal**      | boolean | `false` | `taskinfo.internal`      | `true` → Display Gradle-internal non-task nodes encountered in the task graph.<br>`false` → Omit Gradle-internal non-task nodes. If the internal nodes have dependencies, those will be shown on their parent tasks. |

These options can be set in multiple places, listed in order of precedence:

1. **Command line** – For a single Gradle invocation:
   ```shell
   ./gradlew tiTree assemble -Ptaskinfo.clipped=false -Ptaskinfo.color=true -Ptaskinfo.showTaskTypes=true -Ptaskinfo.internal=false
   ```
2. **gradle.properties file** – You can set them in a *gradle.properties* file, even the global one:
   ```.properties
   taskinfo.clipped = false
   taskinfo.color = true
   taskinfo.showTaskTypes = true
   taskinfo.internal = false
   ```
3. **build.gradle file** – In a *build.gradle* file, you can use the `TaskInfoExtension`:
   ```groovy
   taskinfo {
     clipped = false
     color = true
     showTaskTypes = true
     internal = false
   }
   ```

&nbsp;

----------------------------------------------------------------------------------------------------------------------

## Development

In order to develop *gradle-taskinfo* and build it on your local machine, you need:

- Java 8 JDK ([download](https://adoptium.net/temurin/releases?version=8))
- Gradle, but we use the Gradle Wrapper, so there is nothing to install for you

The project is IDE agnostic. Just use your favorite IDE and make sure none of its control files get checked in.


### Versioning

This project uses [semantic versioning](https://semver.org/spec/v2.0.0.html). The following elements of the plugin
influence the minor and major version:

- The Gradle and Java versions supported
- The tasks provided by this plugin
- The configuration options
- The JSON format of the **tiJson** output
- The information provided by our tasks

The following elements are explicitly not part of the "contract":

- Colors
- Visual appearance of the **tiTree** and **tiOrder** output, which is intended for use by humans.


### Debugging

Since this plugin uses a lot of reflection to get at Gradle internals, it is frequently necessary to debug a Gradle
run which is causing problems. This can help find out how a certain field is called in a particular version of Gradle.  
Debugging a build is mostly explained
[in the docs](https://docs.gradle.org/8.5/userguide/troubleshooting.html#sec:troubleshooting_build_logic). Let me
just add to this that the build to be debugged should be invoked with `gradle someTask -Dorg.gradle.debug=true
--no-daemon`. Without `--no-daemon`, things will likely not work.  
Make sure that the source code of *gradle-taskinfo* in your IDE is at the same version as in the build you are
debugging. In IntelliJ, use the "Remote JVM Debug" run configuration, not "Gradle". It will be pre-configured correctly
by IntelliJ.


## Status

*gradle-taskinfo* is feature-complete and stable. It is ready for enterprise deployment.


## License

*gradle-taskinfo* is free software under the terms of the Apache License, Version 2.0.
Details in the [LICENSE](LICENSE) file.


## Contributing

Contributions of all kinds are very welcome! If you are going to spend a lot of time on your contribution, it may
make sense to raise an issue first and discuss your contribution. Thank you!
