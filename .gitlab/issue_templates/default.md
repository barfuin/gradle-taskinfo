If you are reporting a problem, please be sure to give the following information:

- Java version
- Gradle version
- *gradle-taskinfo* version
- Full stacktrace, if available
- A concise description of how to reproduce the problem.  
  The best such description is an MR with a failing unit test. :slight-smile:
- A description of the desired behavior, if it makes sense for your issue.
- Bonus: A debug level log file of the plugin run leading to the problem

Thank you for putting this kind of effort into the problem report!  
This will make things much clearer for us, which means the problem will be easier and quicker to address.
