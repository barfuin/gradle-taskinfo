/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.barfuin.texttree.api.color.ColorScheme;
import org.barfuin.texttree.api.color.DefaultColorScheme;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Some unit tests of {@link TaskInfoDto}.
 */
public class TaskInfoDtoTest
{
    @Test
    @SuppressWarnings("OverwrittenKey")
    public void testEqualsHashCode()
    {
        final Set<TaskInfoDto> set = new HashSet<>();
        final TaskInfoDto b = new TaskInfoDto(":b", true);

        set.add(null);
        set.add(new TaskInfoDto(":a", true));
        set.add(b);
        set.add(new TaskInfoDto(":c", true));
        set.add(new TaskInfoDto(":a", true));
        set.add(b);
        set.add(null);

        Assertions.assertEquals(4, set.size());  // a, b, c, and null
    }



    @Test
    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions", "EqualsBetweenInconvertibleTypes"})
    public void testEqualsNull()
    {
        final TaskInfoDto underTest = new TaskInfoDto(":task", false);
        Assertions.assertFalse(underTest.equals(null));
        Assertions.assertFalse(underTest.equals("not a TaskInfoDto"));
    }



    @Test
    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    public void testEqualsSelf()
    {
        final TaskInfoDto underTest = new TaskInfoDto(":task", false);
        Assertions.assertTrue(underTest.equals(underTest));
    }



    @Test
    @SuppressWarnings("SimplifiableAssertion")
    public void testEqualsSuccess()
    {
        final TaskInfoDto underTest = new TaskInfoDto(":task", false);
        final TaskInfoDto otherEqual = new TaskInfoDto(":task", false);
        final TaskInfoDto otherDifferent = new TaskInfoDto(":task", true);

        Assertions.assertTrue(underTest.equals(otherEqual));
        Assertions.assertFalse(underTest.equals(otherDifferent));
    }



    /**
     * Test that nodes which do not represent tasks (but, say, transformations or work actions) are colored the same
     * color as annotations. It could be different, but that's a visual design decision we've made.
     */
    @Test
    public void testNonTaskNodes()
    {
        final ColorScheme defaultColorScheme = new DefaultColorScheme();
        final TaskInfoDto underTest = new TaskInfoDto(":task", false, false, true);
        Assertions.assertEquals(defaultColorScheme.getAnnotationColor(), underTest.getColor());
    }



    @Test
    public void testNonTaskNodesNoColor()
    {
        final TaskInfoDto underTest = new TaskInfoDto(":task", false, false, false);
        Assertions.assertNull(underTest.getColor());
    }



    @Test
    public void testNoArgsConstructor()
    {
        TaskInfoDto dto = new TaskInfoDto();
        Assertions.assertEquals("", dto.getPath());
        Assertions.assertFalse(dto.isFinalizer());

        dto.setPath(":unit:test");
        dto.setFinalizer(true);
        Assertions.assertEquals(":unit:test", dto.getPath());
        Assertions.assertTrue(dto.isFinalizer());
    }



    @Test
    public void testDeserializeJson()
        throws IOException
    {
        TaskInfoDto dto = null;
        try (InputStream is = getClass().getResourceAsStream("taskinfo-assemble.json")) {
            dto = new ObjectMapper().readValue(is, TaskInfoDto.class);
        }

        Assertions.assertNotNull(dto);
        Assertions.assertEquals("assemble", dto.getName());
        Assertions.assertEquals(":assemble", dto.getPath());
        Assertions.assertFalse(dto.isFinalizer());
        Assertions.assertEquals("build", dto.getGroup());
        Assertions.assertEquals("org.gradle.api.DefaultTask", dto.getType());
        Assertions.assertEquals(9, dto.getQueuePosition());
        Assertions.assertNotNull(dto.getDependencies());
        Assertions.assertEquals(3, dto.getDependencies().size());
    }
}
