/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.gradle.api.GradleException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Some unit tests of {@link ReflectUtil}.
 */
public class ReflectUtilTest
{
    @SuppressWarnings({"unused", "FieldMayBeFinal", "RedundantSuppression"})
    private static class TestObject
    {
        private String stringWhichIsNull = null;
    }



    private static class TestList
        extends ArrayList<String>
    {
        // adds nothing
    }



    @Test
    public void testReadNonExistentField()
    {
        try {
            ReflectUtil.readField("myObject", "test object", Arrays.asList("nonExistent1", "nonExistent2"));
            Assertions.fail("expected exception was not thrown");
        }
        catch (GradleException e) {
            Assertions.assertTrue(e.getMessage().contains("Failed to access test object in Gradle on object of type "
                + "'java.lang.String'. When this happens, it normally means you are using a new version of Gradle "
                + "which is not supported by this plugin yet."));
        }
    }



    @Test
    public void testNullObjectHasNoField()
    {
        Assertions.assertFalse(ReflectUtil.hasField(null, "fieldName"));
    }



    @Test
    public void testFieldExistsButIsNull()
    {
        Assertions.assertTrue(ReflectUtil.hasField(new TestObject(), "stringWhichIsNull"));
    }



    @Test
    public void testFieldExistsButIsNull2()
    {
        try {
            ReflectUtil.readField(new TestObject(), "test object", Collections.singletonList("stringWhichIsNull"));
            Assertions.fail("expected exception was not thrown");
        }
        catch (GradleException e) {
            Assertions.assertTrue(e.getMessage().contains("Failed to access test object in Gradle on object of type "
                + "'org.barfuin.gradle.taskinfo.util.ReflectUtilTest$TestObject'. When this happens, it normally "
                + "means you are using a new version of Gradle which is not supported by this plugin yet."));
        }
    }



    @Test
    public void testNoFieldNamesSpecified()
    {
        try {
            ReflectUtil.readField("myObject", "test object", Collections.emptyList());
            Assertions.fail("expected exception was not thrown");
        }
        catch (GradleException e) {
            // When this happens, it's really a bug, but well.
            Assertions.assertTrue(e.getMessage().contains("Failed to access test object in Gradle on object of type "
                + "'java.lang.String'. When this happens, it normally means you are using a new version of Gradle "
                + "which is not supported by this plugin yet."));
        }
    }



    @Test
    public void testMultipleFieldNamesSpecified()
    {
        final Integer hash = (Integer)
            ReflectUtil.readField("myObject", "test object", Arrays.asList("nonExistent", "nonExistent2", "hash"));
        Assertions.assertEquals(0, hash.intValue());
    }



    @Test
    public void testNonExistentField()
    {
        Assertions.assertFalse(ReflectUtil.hasField(new TestObject(), "NON_EXISTENT"));
    }



    @Test
    public void testCallMethodIfPresentNonExistent()
    {
        Assertions.assertNull(ReflectUtil.callMethodIfPresent("test object", "nonExistent"));
    }



    @Test
    public void testCallMethodIfPresentNoObject()
    {
        Assertions.assertNull(ReflectUtil.callMethodIfPresent(null, "length"));
    }



    @Test
    public void testCallMethodIfPresentSuccess1()
    {
        // works if the method is defined in the class itself
        Assertions.assertEquals(11, ReflectUtil.callMethodIfPresent("test object", "length"));
    }



    @Test
    public void testCallMethodIfPresentSuccess2()
    {
        // works also if the method is defined in a superclass
        Assertions.assertEquals(0, ReflectUtil.callMethodIfPresent(new TestList(), "size"));
    }



    /**
     * Test dummy for the {@link #testReflectiveException()} test case.
     */
    public static class Dummy
    {
        @SuppressWarnings("unused")
        public void method()
        {
            throw new IllegalArgumentException("thrown for testing only, not an error");
        }
    }



    @Test
    public void testReflectiveException()
    {
        Assertions.assertNull(ReflectUtil.callMethodIfPresent(new Dummy(), "method"));
    }



    @Test
    public void testLoadClassNotFound()
    {
        Assertions.assertNull(ReflectUtil.loadClassIfPossible("nonExistent"));
    }



    @Test
    public void testNullIsNotAnInstance()
    {
        Assertions.assertFalse(ReflectUtil.isInstanceOf("java.lang.String", null));
    }
}
