/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.util.Collection;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.gradle.taskinfo.GradleTaskInfoPlugin;
import org.barfuin.gradle.taskinfo.TaskInfoDto;
import org.gradle.api.Project;
import org.gradle.api.internal.tasks.NodeExecutionContext;
import org.gradle.api.internal.tasks.TaskDependencyResolveContext;
import org.gradle.api.internal.tasks.WorkNodeAction;
import org.gradle.execution.plan.ActionNode;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Some supplementary unit tests of {@link TaskNodeHolder}.
 */
public class TaskNodeHolderTest
{
    /**
     * A dummy implementation of {@link WorkNodeAction} for testing.
     */
    private static class TestAction
        implements WorkNodeAction
    {
        private final Project project;



        public TestAction(final Project pProject)
        {
            Assertions.assertNotNull(pProject);
            project = pProject;
        }



        @Nullable
        @Override
        public Project getOwningProject()
        {
            return project;
        }



        @Override
        public boolean usesMutableProjectState()
        {
            return false;
        }



        @Override
        public void visitDependencies(@Nonnull final TaskDependencyResolveContext pTaskDependencyResolveContext)
        {
            project.getLogger().lifecycle("visitDependencies() was called");
        }



        @Override
        public void run(@Nonnull final NodeExecutionContext pNodeExecutionContext)
        {
            project.getLogger().lifecycle("Hello from the dummy test action.");
        }



        @Override
        public String toString()
        {
            return "for testing";
        }
    }



    /**
     * Simulates a legacy action node which contains an action that has no project affiliation.
     */
    private static class TestActionNode
    {
        @SuppressWarnings({"FieldMayBeFinal", "unused"})
        private String action = "NOT REALLY A VALID ACTION";



        @SuppressWarnings("unused")
        public void run(@Nonnull final NodeExecutionContext pNodeExecutionContext)
        {
            // do nothing
        }



        @Override
        public String toString()
        {
            return "work action for testing";
        }
    }



    @Test
    public void testActionNode()
    {
        final Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(GradleTaskInfoPlugin.PLUGIN_ID);
        WorkNodeAction action = new TestAction(project);
        ActionNode actionNode = new ActionNode(action);

        final TaskNodeHolder underTest = new TaskNodeHolder(project, actionNode);

        assertActionNode(underTest, ":", ActionNode.class);
    }



    @Test
    public void testActionNodeSubproject()
    {
        final Project root = ProjectBuilder.builder().build();
        final Project project = ProjectBuilder.builder().withParent(root).withName("sub").build();
        project.getPlugins().apply(GradleTaskInfoPlugin.PLUGIN_ID);
        WorkNodeAction action = new TestAction(project);
        ActionNode actionNode = new ActionNode(action);

        final TaskNodeHolder underTest = new TaskNodeHolder(project, actionNode);

        assertActionNode(underTest, ":sub:", ActionNode.class);
    }



    @Test
    public void testActionNodeLegacy()
    {
        final Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(GradleTaskInfoPlugin.PLUGIN_ID);
        TestActionNode actionNode = new TestActionNode();

        final TaskNodeHolder underTest = new TaskNodeHolder(project, actionNode);

        assertActionNode(underTest, "", TestActionNode.class);
    }



    private void assertActionNode(final TaskNodeHolder pUnderTest, final String pPathPrefix,
        final Class<?> pDelegateClass)
    {
        final Pattern workNodeActionPattern = Pattern.compile(pPathPrefix + "workNodeAction[0-9a-f]+");
        Assertions.assertNull(pUnderTest.getTask());
        Assertions.assertEquals(pDelegateClass, pUnderTest.getType());
        Assertions.assertFalse(pUnderTest.isOneOfOurTasks());
        assertEmptyCollection(pUnderTest.getFinalizers());
        assertEmptyCollection(pUnderTest.getDependencySuccessors());
        Assertions.assertEquals("work action for testing", pUnderTest.getDisplayName());
        final String identity = pUnderTest.getIdentity();
        Assertions.assertNotNull(identity);
        Assertions.assertTrue(workNodeActionPattern.matcher(identity).matches());

        final TaskInfoDto dto = pUnderTest.asTaskInfoDto(false, true);
        Assertions.assertEquals("work action for testing", dto.getName());
        Assertions.assertNotNull(dto.getPath());
        Assertions.assertTrue(workNodeActionPattern.matcher(dto.getPath()).matches());
        Assertions.assertFalse(dto.isFinalizer());
        Assertions.assertNull(dto.getGroup());
        Assertions.assertEquals(pDelegateClass.getName(), dto.getType());
        Assertions.assertEquals(-1, dto.getQueuePosition());
        assertEmptyCollection(dto.getDependencies());
    }



    private void assertEmptyCollection(@Nullable final Collection<?> pCollection)
    {
        Assertions.assertNotNull(pCollection);
        Assertions.assertEquals(0, pCollection.size());
    }
}
