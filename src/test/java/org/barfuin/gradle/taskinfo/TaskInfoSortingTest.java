/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Unit tests of {@link TaskInfoSorting}.
 */
public class TaskInfoSortingTest
{
    @Test
    public void testSimple()
    {
        final TaskInfoSorting underTest = new TaskInfoSorting();

        Assertions.assertEquals(-1, underTest.compare(
            new TaskInfoDto(":foo", false), new TaskInfoDto(":project1:foo", false)));
        Assertions.assertEquals(1, underTest.compare(
            new TaskInfoDto(":project1:foo", false), new TaskInfoDto(":foo", false)));
        Assertions.assertEquals(0, underTest.compare(
            new TaskInfoDto(":foo", false), new TaskInfoDto(":foo", false)));
    }



    @Test
    @SuppressWarnings("EqualsWithItself")
    public void testNulls()
    {
        final TaskInfoSorting underTest = new TaskInfoSorting();

        Assertions.assertEquals(-1, underTest.compare(new TaskInfoDto(":foo", false), null));
        Assertions.assertEquals(1, underTest.compare(null, new TaskInfoDto(":foo", false)));
        Assertions.assertEquals(0, underTest.compare(null, null));
    }



    @Test
    public void testNesting()
    {
        final TaskInfoSorting underTest = new TaskInfoSorting();

        Assertions.assertTrue(underTest.compare(
            new TaskInfoDto(":project1:sub1:foo", false), new TaskInfoDto(":project1:sub2:foo", false)) < 0);
        Assertions.assertTrue(underTest.compare(
            new TaskInfoDto(":project1:foo", false), new TaskInfoDto(":project1:bar", false)) > 0);
        Assertions.assertEquals(0, underTest.compare(
            new TaskInfoDto("project1:sub1:foo", false), new TaskInfoDto(":project1:sub1:foo", false)));
    }



    @Test
    public void testMissingColons()
    {
        final TaskInfoSorting underTest = new TaskInfoSorting();

        Assertions.assertEquals(0, underTest.compare(
            new TaskInfoDto(":foo", false), new TaskInfoDto("foo", false)));
        Assertions.assertEquals(0, underTest.compare(
            new TaskInfoDto(":project1:foo", false), new TaskInfoDto("project1:foo", false)));
        Assertions.assertTrue(underTest.compare(
            new TaskInfoDto("bar", false), new TaskInfoDto("foo", false)) < 0);
    }



    @Test
    public void testWithFinalizerTasks()
    {
        final SortedSet<TaskInfoDto> sortedSet = new TreeSet<>();
        sortedSet.add(new TaskInfoDto(":foo", false));
        sortedSet.add(new TaskInfoDto(":project1:foo", false));
        sortedSet.add(new TaskInfoDto(":project1:bar", false));
        sortedSet.add(new TaskInfoDto(":project1:subproject1:foo", false));
        sortedSet.add(new TaskInfoDto(":project2:subproject1:zoo", false));
        sortedSet.add(new TaskInfoDto(":project1:subproject1:bar", false));
        sortedSet.add(new TaskInfoDto(":project1:subproject1:boo", true));
        sortedSet.add(new TaskInfoDto(":project1:subproject2:boo", true));
        sortedSet.add(new TaskInfoDto(":project1:subproject2:foo", false));
        sortedSet.add(new TaskInfoDto(":zoo", false));
        sortedSet.add(new TaskInfoDto(":a", false));
        sortedSet.add(new TaskInfoDto(":argh", true));

        final String actual = sortedSet.stream()
            .map(ti -> ti.getPath() + (ti.isFinalizer() ? " (finalizer)" : ""))
            .collect(Collectors.joining(", "));
        Assertions.assertEquals(
            ":a, "
                + ":foo, "
                + ":zoo, "
                + ":argh (finalizer), "
                + ":project1:bar, "
                + ":project1:foo, "
                + ":project1:subproject1:bar, "
                + ":project1:subproject1:foo, "
                + ":project1:subproject1:boo (finalizer), "
                + ":project1:subproject2:foo, "
                + ":project1:subproject2:boo (finalizer), "
                + ":project2:subproject1:zoo",
            actual);
    }
}
