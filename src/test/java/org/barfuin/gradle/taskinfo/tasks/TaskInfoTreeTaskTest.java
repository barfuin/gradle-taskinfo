/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.tasks;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.barfuin.gradle.taskinfo.Fixtures;
import org.barfuin.gradle.taskinfo.GradleTaskInfoPlugin;
import org.barfuin.gradle.taskinfo.TaskInfoDto;
import org.barfuin.gradle.taskinfo.TaskInfoExtension;
import org.barfuin.gradle.taskinfo.TaskProbe;
import org.barfuin.gradle.taskinfo.test.LogXformUtil;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.internal.TaskInternal;
import org.gradle.api.logging.Logger;
import org.gradle.api.problems.Problems;
import org.gradle.execution.plan.LocalTaskNode;
import org.gradle.internal.execution.WorkValidationContext;
import org.gradle.internal.execution.impl.DefaultWorkValidationContext;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;


/**
 * Some supplementary unit tests for {@link TaskInfoTreeTask} which rely on Mockito instead of Gradle TestKit.
 */
public class TaskInfoTreeTaskTest
{
    @Test
    public void testUnknownNodeType()
    {
        final ArgumentCaptor<String> warnings = ArgumentCaptor.forClass(String.class);
        final Project project = createProjectWithWarningCaptor(warnings);

        final TaskInfoExtension config = new TaskInfoExtension();
        config.setInternal(true);  // else the unknown node would be suppressed

        final TaskInfoTreeTask underTest = prepareTaskUnderTest(project, config);
        final Task taskB = project.getTasks().create("taskB_0123456789012345678901234");
        final Task taskA = project.getTasks().create("taskA");
        taskA.dependsOn(taskB);
        final TaskNodeHolder holderB = buildMockHolder(project, (TaskInternal) taskB);
        final TaskNodeHolder holderU = new TaskNodeHolder(project, "unknown node type (here: String)");

        final TaskNodeHolder holderA = mockTaskNode(config, taskA, Arrays.asList(holderB, holderU));
        underTest.setEntryNode(holderA);

        final List<TaskNodeHolder> queue = Arrays.asList(holderB, holderU, holderA);
        final TaskProbe probe = Fixtures.buildTaskProbe(project, config, queue);
        Mockito.when(underTest.buildTaskProbe(Mockito.any(TaskInfoExtension.class))).thenReturn(probe);

        ArgumentCaptor<String> resultTree = ArgumentCaptor.forClass(String.class);
        Logger logger = Mockito.mock(Logger.class);
        Mockito.doNothing().when(logger).lifecycle(resultTree.capture());
        Mockito.doNothing().when(logger).warn(warnings.capture());
        Mockito.when(underTest.getLogger()).thenReturn(logger);

        underTest.executeTaskInfo();

        Assertions.assertTrue(warnings.getValue().contains("WARNING: While working with Gradle's internal execution "
            + "queue and task dependencies, we encountered a node of type 'java.lang.String'"));

        final String actual = LogXformUtil.normalizeAddresses(LogXformUtil.unixifyLineBreaks(resultTree.getValue()));
        final String expected = ":taskA\u001B[90m                                (org.gradle.api.DefaultTask)\u001B[m\n"
            + "\u001B[33m+--- \u001B[m:taskB_0123456789012345678901234\u001B[90m (org.gradle.api.DefaultTask)\u001B[m\n"
            + "\u001B[33m`--- \u001B[90munknownNode_xxxxxxx              (java.lang.String)\u001B[m\n";
        Assertions.assertEquals(expected, actual);
    }



    @SuppressWarnings({"UnstableApiUsage", "unchecked"})
    private TaskNodeHolder buildMockHolder(final Project pProject, final TaskInternal pTask)
    {
        return new TaskNodeHolder(pProject, new LocalTaskNode(pTask,
            new DefaultWorkValidationContext(Mockito.mock(WorkValidationContext.TypeOriginInspector.class),
                Mockito.mock(Problems.class)), Mockito.mock(Function.class)));
    }



    private Project createProjectWithWarningCaptor(final ArgumentCaptor<String> pWarnings)
    {
        final Project result = Mockito.spy(ProjectBuilder.builder().build());

        Logger prjLogger = Mockito.mock(Logger.class);
        Mockito.doNothing().when(prjLogger).warn(pWarnings.capture());
        Mockito.when(result.getLogger()).thenReturn(prjLogger);

        result.getPlugins().apply(GradleTaskInfoPlugin.PLUGIN_ID);
        Assertions.assertNotNull(result.getTasks().findByName(GradleTaskInfoPlugin.TASKINFO_TASK_NAME));

        return result;
    }



    private TaskInfoTreeTask prepareTaskUnderTest(final Project pProject, final TaskInfoExtension pConfig)
    {
        final TaskInfoTreeTask result = Mockito.mock(TaskInfoTreeTask.class);
        Mockito.doCallRealMethod().when(result).setEntryNode(Mockito.any(TaskNodeHolder.class));
        Mockito.doCallRealMethod().when(result).getEntryNode();
        Mockito.doCallRealMethod().when(result).setEntryNodeLabel(Mockito.any(String.class));
        Mockito.doCallRealMethod().when(result).getEntryNodeLabel();
        Mockito.doCallRealMethod().when(result).executeTaskInfo();
        Mockito.doNothing().when(result).validatePreconditions();
        Mockito.when(result.getProject()).thenReturn(pProject);
        Mockito.when(result.getConfig()).thenReturn(pConfig);
        return result;
    }



    private TaskNodeHolder mockTaskNode(final TaskInfoExtension pConfig, final Task pTask,
        final List<TaskNodeHolder> pDependencies)
    {
        final TaskNodeHolder result = Mockito.mock(TaskNodeHolder.class);
        Mockito.when(result.getIdentity()).thenReturn(pTask.getPath());
        Mockito.when(result.getDisplayName()).thenReturn(pTask.getName());
        Mockito.when(result.getTask()).thenReturn(pTask);
        Mockito.when(result.getType()).then(invocation -> DefaultTask.class);
        Mockito.when(result.getDependencySuccessors()).thenReturn(pDependencies);
        Mockito.when(result.getFinalizers()).thenReturn(Collections.emptyList());
        Mockito.when(result.asTaskInfoDto(Mockito.anyBoolean(), Mockito.anyBoolean())).then(invocation -> {
            TaskInfoDto dto = new TaskInfoDto(pTask.getPath(), false, true, pConfig.isColor());
            dto.setName(pTask.getName());
            dto.setType(DefaultTask.class.getName());
            return dto;
        });
        return result;
    }
}
