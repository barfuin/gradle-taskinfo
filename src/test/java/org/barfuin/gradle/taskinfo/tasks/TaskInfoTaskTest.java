/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.tasks;

import org.barfuin.gradle.taskinfo.GradleTaskInfoPlugin;
import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Some unit tests of {@link AbstractInfoTask}.
 */
public class TaskInfoTaskTest
{
    @Test
    public void testEntryNodeLabelNull()
    {
        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply(GradleTaskInfoPlugin.PLUGIN_ID);

        AbstractInfoTask task = (AbstractInfoTask)
            project.getTasks().findByName(GradleTaskInfoPlugin.TASKINFO_TASK_NAME);
        Assertions.assertNotNull(task);

        task.setEntryNodeLabel(null);
        Assertions.assertEquals("<unknown>", task.getEntryNodeLabel());
    }
}
