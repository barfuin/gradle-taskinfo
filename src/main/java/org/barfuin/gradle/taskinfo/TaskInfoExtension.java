/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.Serializable;


/**
 * Gradle extension for configuring this plugin.
 */
public class TaskInfoExtension
    implements Serializable
{
    private static final long serialVersionUID = 3L;

    private boolean clipped = false;

    private boolean color = true;

    private boolean showTaskTypes = true;

    private boolean internal = false;



    public boolean isClipped()
    {
        return clipped;
    }



    public void setClipped(final boolean pClipped)
    {
        clipped = pClipped;
    }



    public boolean isColor()
    {
        return color;
    }



    public void setColor(final boolean pColor)
    {
        color = pColor;
    }



    public boolean isShowTaskTypes()
    {
        return showTaskTypes;
    }



    public void setShowTaskTypes(final boolean pShowTaskTypes)
    {
        showTaskTypes = pShowTaskTypes;
    }



    public boolean isInternal()
    {
        return internal;
    }



    public void setInternal(final boolean pInternal)
    {
        internal = pInternal;
    }
}
