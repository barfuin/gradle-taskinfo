/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.util.ReflectUtil;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.Project;
import org.gradle.api.execution.TaskExecutionGraph;


/**
 * Detects the task dependencies and metadata using dark magic.
 */
public class TaskProbe
{
    private final Project project;

    private final TaskInfoExtension config;

    private List<TaskNodeHolder> executionQueue = null;

    private boolean nonTaskNodesPresent = false;

    private boolean tasksFromAnotherBuildPresent = false;



    public TaskProbe(@Nonnull final Project pProject, @Nonnull final TaskInfoExtension pConfig)
    {
        project = Objects.requireNonNull(pProject, "Bug: constructor parameter 'pProject' was null");
        config = Objects.requireNonNull(pConfig, "Bug: constructor parameter 'pConfig' was null");
    }



    TaskProbe(@Nonnull final Project pProject, @Nonnull final TaskInfoExtension pConfig,
        final List<TaskNodeHolder> pExecutionQueue)
    {
        this(pProject, pConfig);
        executionQueue = pExecutionQueue;
    }



    @Nonnull
    public TaskInfoDto buildHierarchy(@Nonnull final TaskNodeHolder pEntryNode)
    {
        Objects.requireNonNull(pEntryNode, "Bug: Argument 'pEntryNode' was null");
        executionQueue = readExecutionQueue();
        final Map<String, Integer> queueIndex = indexQueue(executionQueue);
        TaskInfoDto taskInfo = traverse(pEntryNode, queueIndex, false, new ArrayDeque<>());
        return taskInfo;
    }



    @Nonnull
    public List<TaskNodeHolder> buildOrder()
    {
        return readExecutionQueue();
    }



    private List<TaskNodeHolder> readExecutionQueue()
    {
        if (executionQueue != null) {
            return executionQueue;
        }
        final TaskExecutionGraph taskGraph = project.getGradle().getTaskGraph();
        final Object executionPlan = ReflectUtil.getExecutionPlan(taskGraph);
        return getExecutionQueue(executionPlan);
    }



    @Nonnull
    private List<TaskNodeHolder> getExecutionQueue(@Nonnull final Object pExecutionPlan)
    {
        final Iterable<?> rawQueue = getRawQueue(pExecutionPlan);
        List<TaskNodeHolder> result = new ArrayList<>();
        for (Object element : rawQueue) {
            if (!TaskNodeHolder.hasRelevantNodeType(element)) {
                continue;
            }
            TaskNodeHolder holder = new TaskNodeHolder(project, element);
            holder.getIdentity(); // determine node type
            if (holder.isInternalNode() && !config.isInternal()) {
                continue;
            }
            if (holder.getTask() == null) {
                nonTaskNodesPresent = true;
            }
            if (!holder.isOneOfOurTasks()) {
                result.add(holder);
            }
        }
        return result;
    }



    @Nonnull
    private Iterable<?> getRawQueue(@Nonnull final Object pExecutionPlan)
    {
        Object obj = ReflectUtil.readField(pExecutionPlan, "node mapping",
            Arrays.asList("nodeMapping", "workInfoMapping", "executionPlan"));
        Iterable<?> rawQueue;
        if (obj instanceof Map<?, ?>) {
            rawQueue = ((Map<?, ?>) obj).values();
        }
        else {
            rawQueue = (Iterable<?>) obj;
        }
        return rawQueue;
    }



    private Map<String, Integer> indexQueue(@Nonnull final List<TaskNodeHolder> pExecutionQueue)
    {
        Map<String, Integer> result = new HashMap<>(pExecutionQueue.size() + 1);
        for (int idx = 0; idx < pExecutionQueue.size(); idx++) {
            String path = pExecutionQueue.get(idx).getIdentity();
            result.put(path, Integer.valueOf(idx));
        }
        return result;
    }



    @Nonnull
    private TaskInfoDto traverse(@Nonnull final TaskNodeHolder pTaskNode,
        @Nonnull final Map<String, Integer> pQueueIndex, final boolean pFinalizer,
        @Nonnull final Deque<String> pPreviousNodeStack)
    {
        final String path = pTaskNode.getIdentity();
        if (pTaskNode.isInternalNode()) {
            nonTaskNodesPresent = true;
        }
        if (pTaskNode.isTaskInAnotherBuild()) {
            tasksFromAnotherBuildPresent = true;
        }

        final TaskInfoDto result = pTaskNode.asTaskInfoDto(pFinalizer, config.isColor());
        if (pQueueIndex.containsKey(path)) {
            result.setQueuePosition(pQueueIndex.get(path).intValue() + 1);
        }

        if (!pPreviousNodeStack.contains(path)) {
            pPreviousNodeStack.push(path);
            for (TaskNodeHolder tn : pTaskNode.getDependencySuccessors()) {
                addDependency(result, traverse(tn, pQueueIndex, false, pPreviousNodeStack));
            }
            for (TaskNodeHolder tn : pTaskNode.getFinalizers()) {
                addDependency(result, traverse(tn, pQueueIndex, true, pPreviousNodeStack));
            }
            pPreviousNodeStack.pop();
        }
        return result;
    }



    private void addDependency(@Nonnull final TaskInfoDto pNode, @Nonnull final TaskInfoDto pDependency)
    {
        if (pDependency.isTaskNode() || config.isInternal() || isTaskInAnotherBuild(pDependency)) {
            pNode.addDependency(pDependency);
        }
        else {
            for (TaskInfoDto dep : pDependency.getDependencies()) {
                addDependency(pNode, dep);
            }
        }
    }



    private boolean isTaskInAnotherBuild(@Nonnull final TaskInfoDto pNode)
    {
        /* Depending on the Gradle version, the type may be "org.gradle.execution.plan.TaskInAnotherBuild" or
         * "org.gradle.execution.plan.TaskInAnotherBuild$1".
         */
        return pNode.getType() != null && pNode.getType().startsWith("org.gradle.execution.plan.TaskInAnotherBuild");
    }



    /**
     * Getter.
     *
     * @return flag indicating if any nodes in the result are not task nodes. This is only set after a buildXxx()
     * method was executed.
     */
    public boolean isNonTaskNodesPresent()
    {
        return nonTaskNodesPresent;
    }



    public boolean isAnyTaskFromAnotherBuild()
    {
        return tasksFromAnotherBuildPresent;
    }
}
