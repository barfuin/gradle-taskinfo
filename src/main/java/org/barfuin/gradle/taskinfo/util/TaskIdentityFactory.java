/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.util.Collections;
import java.util.Objects;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.internal.TaskInternal;
import org.gradle.util.GradleVersion;


/**
 * Creates an identity String (normally a task path, like {@code ":module:myTask"}) for a node in the Gradle execution
 * plan. The factory must be able to handle all the subtypes of {@code org.gradle.execution.plan.Node}.
 */
public class TaskIdentityFactory
{
    private final Project project;

    private boolean wasInternalNode = false;

    private boolean wasTaskInAnotherBuild = false;



    public TaskIdentityFactory(@Nonnull final Project pProject)
    {
        project = Objects.requireNonNull(pProject, "pProject was null");
    }



    @Nonnull
    public String forNode(@Nonnull final Object pNode)
    {
        // OrdinalNodes are ignored
        if (pNode instanceof Task) {
            return forTaskNode((Task) pNode);
        }
        final Task task = tryReadTaskField(pNode);
        if (task != null) {
            return forTaskNode(task);
        }
        else if (ReflectUtil.hasField(pNode, "action")) {
            wasInternalNode = true;
            return forActionNode(pNode);
        }
        else if (ReflectUtil.hasField(pNode, "node")) {
            wasInternalNode = true;
            return forResolveMutationsNode(pNode);
        }
        else if (ReflectUtil.hasField(pNode, "taskIdentityPath") || ReflectUtil.hasField(pNode, "task")) {
            wasTaskInAnotherBuild = true;   // NOT an internal node
            if (ReflectUtil.hasField(pNode, "task")) {
                return forTaskNode((Task)   // org.gradle.execution.plan.TaskInAnotherBuild$Resolved
                    ReflectUtil.readField(pNode, "task field", Collections.singletonList("task")));
            }
            else {
                return forTaskInAnotherBuild(pNode);
            }
        }
        String result = tryForTransformationNode(pNode);

        if (result == null) {
            // ok, now we encountered something new, which we'll report as an unknown node
            wasInternalNode = true;
            result = "unknownNode" + getIdHash(pNode);
            project.getLogger().warn("WARNING: While working with Gradle's internal execution queue and task "
                + "dependencies, we encountered a node of type '" + pNode.getClass().getName() + "' which is "
                + "either an unknown type, or does not have expected fields. When this happens, it normally means you "
                + "are using a new version of Gradle which is not fully supported by this plugin yet. You are using "
                + GradleVersion.current().toString()
                + ". Open an issue at https://gitlab.com/barfuin/gradle-taskinfo/-/issues. Thank you!");
        }
        return result;
    }



    @CheckForNull
    private Task tryReadTaskField(@Nonnull final Object pNode)
    {
        if (ReflectUtil.hasField(pNode, "task")
            && !ReflectUtil.isInstanceOf("org.gradle.execution.plan.TaskInAnotherBuild", pNode)
            && !(ReflectUtil.isInstanceOf("org.gradle.execution.plan.TaskNode", pNode)
                && "TaskInAnotherBuild".equals(pNode.getClass().getSimpleName()))
            && !(ReflectUtil.isInstanceOf("org.gradle.execution.taskgraph.TaskInfo", pNode)
                && "TaskInAnotherBuild".equals(pNode.getClass().getSimpleName())))
        {
            Task result = (Task) ReflectUtil.readField(pNode, "task field", Collections.singletonList("task"));
            if (!(ReflectUtil.isInstanceOf("org.gradle.execution.taskgraph.TaskInfo", pNode)
                && ReflectUtil.isInstanceOf("org.gradle.composite.internal.CompositeBuildTaskDelegate", result))) {
                return result;
            }
        }
        return null;
    }



    private String getPathWithSeparator(final Project pProject)
    {
        String result = pProject.getPath();
        if (!Project.PATH_SEPARATOR.equals(result)) {
            result += Project.PATH_SEPARATOR;
        }
        return result;
    }



    @Nonnull
    private String forTaskNode(@Nonnull final Task pTask)
    {
        return ((TaskInternal) pTask).getIdentityPath().getPath();
    }



    @Nonnull
    private String forActionNode(@Nonnull final Object pNode)
    {
        String result = null;
        final Object action =
            ReflectUtil.readField(pNode, "work node action", Collections.singletonList("action"), true);
        if (action != null) {
            Project owningProject = (Project) ReflectUtil.callMethodIfPresent(action, "getProject");
            if (owningProject != null) {
                result = getPathWithSeparator(owningProject);
            }
            else {
                owningProject = (Project) ReflectUtil.callMethodIfPresent(action, "getOwningProject");
                if (owningProject != null) {
                    result = getPathWithSeparator(owningProject);
                }
            }
        }
        if (result == null) {
            result = "";
        }
        result += "workNodeAction" + getIdHash(pNode);
        return result;
    }



    @Nonnull
    private String forResolveMutationsNode(@Nonnull final Object pResolveMutationsNode)
    {
        final String idHash = getIdHash(pResolveMutationsNode);
        final Object taskNode =
            ReflectUtil.readField(pResolveMutationsNode, "task node", Collections.singletonList("node"), true);
        if (taskNode != null) {
            final Task task = tryReadTaskField(taskNode);
            if (task != null) {
                return forTaskNode(task) + " (resolveMutations" + idHash + ")";
            }
        }
        return "resolveMutations" + idHash;
    }



    @Nonnull
    private String forTaskInAnotherBuild(@Nonnull final Object pNodeInAnotherBuild)
    {
        final Object taskId = ReflectUtil.readField(pNodeInAnotherBuild, "task identity path",
            Collections.singletonList("taskIdentityPath"), false);
        return Objects.requireNonNull(taskId).toString();
    }



    @CheckForNull
    private String tryForTransformationNode(@Nonnull final Object pTransformationNode)
    {
        // Transformation nodes do not require project state (so says the Gradle source code), so if we can't
        // find a project, we can leave it out if it is otherwise ensured that we are a transformation node
        // (before 6.0, it is impossible to find out the project, because no 'owningProject').
        Object projectInternal = ReflectUtil.callMethodIfPresent(pTransformationNode, "getOwningProject");
        String result = (String) ReflectUtil.callMethodIfPresent(projectInternal, "getPath");
        if (result != null) {
            if (!Project.PATH_SEPARATOR.equals(result)) {
                result += Project.PATH_SEPARATOR;
            }
        }
        else if (isTransformNodeForSure(pTransformationNode)) {
            // in Gradle 5.x, TransformationNodes carried no project information, but the class already existed
            result = "";
        }
        if (result != null) {
            wasInternalNode = true;
            result += "transformationStep" + getIdHash(pTransformationNode);
        }
        return result;
    }



    private boolean isTransformNodeForSure(@Nonnull final Object pObject)
    {
        final Class<?> transformNodeClass = ReflectUtil.loadClassIfPossible(
            "org.gradle.api.internal.artifacts.transform.TransformationNode");
        return transformNodeClass != null && transformNodeClass.isAssignableFrom(pObject.getClass());
    }



    private String getIdHash(@Nonnull final Object pObject)
    {
        final int minLen = 8;
        String idHash = Integer.toHexString(System.identityHashCode(pObject));
        if (idHash.length() < minLen) {
            idHash = "00000000".substring(0, minLen - idHash.length()) + idHash;
        }
        return idHash;
    }



    public boolean wasInternalNode()
    {
        return wasInternalNode;
    }



    public boolean wasTaskInAnotherBuild()
    {
        return wasTaskInAnotherBuild;
    }
}
