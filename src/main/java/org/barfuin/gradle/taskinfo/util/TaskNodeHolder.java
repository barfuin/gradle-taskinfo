/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import org.barfuin.gradle.taskinfo.TaskInfoDto;
import org.barfuin.gradle.taskinfo.tasks.AbstractInfoTask;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.internal.TaskInternal;
import org.gradle.api.internal.project.taskfactory.TaskIdentity;
import org.gradle.api.tasks.TaskDependency;


/**
 * Abstracts from changing Gradle classes about internal task nodes. For example,
 * <code>org.gradle.execution.plan.TaskNode</code> used to be <code>org.gradle.execution.taskgraph.TaskInfo</code> in
 * older Gradle. We just want some data out of them.
 */
@NotThreadSafe
public class TaskNodeHolder
{
    /**
     * When reading the execution queue or the task graph, encountered nodes of these types will be ignored.
     * <ul><li><code>OrdinalNode</code>s can be ignored because they exist (since Gradle 7.2 or something) to
     * ensure proper task ordering when more than one task is specified as entry node (e.g. <code>gradlew clean
     * build</code>). We only report on the first entry node anyway, so the single <code>OrdinalNode</code> we
     * always encounter is redundant and without meaning in our context.</li></ul>
     */
    private static final List<String> IGNORED_NODE_TYPES =
        Collections.singletonList("org.gradle.execution.plan.OrdinalNode");

    private final Project project;

    private final Object delegate;

    private final Task task;

    private String identity = null;

    private boolean isInternalNode = false;

    private boolean isTaskInAnotherBuild = false;



    public TaskNodeHolder(@Nonnull final Project pProject, @Nonnull final Object pDelegate)
    {
        project = Objects.requireNonNull(pProject, "pProject was null");
        delegate = Objects.requireNonNull(pDelegate, "pDelegate was null");
        if (pDelegate instanceof Task) {
            task = (Task) pDelegate;
        }
        else if (ReflectUtil.hasField(pDelegate, "task")) {
            task = (Task) ReflectUtil.readField(delegate, "task field", Collections.singletonList("task"));
        }
        else {
            task = null;
        }
    }



    public static boolean hasRelevantNodeType(@Nonnull final Object pNode)
    {
        return !IGNORED_NODE_TYPES.contains(pNode.getClass().getName());
    }



    @CheckForNull
    public Task getTask()
    {
        return task;
    }



    public boolean isOneOfOurTasks()
    {
        return getTask() instanceof AbstractInfoTask;
    }



    public boolean isInternalNode()
    {
        if (identity == null) {
            throw new IllegalStateException("Bug: isInternalNode() called before getIdentity()");
        }
        return isInternalNode;
    }



    public boolean isTaskInAnotherBuild()
    {
        if (identity == null) {
            throw new IllegalStateException("Bug: isTaskInAnotherBuild() called before getIdentity()");
        }
        return isTaskInAnotherBuild;
    }



    @Nonnull
    public String getIdentity()
    {
        if (identity == null) {
            final TaskIdentityFactory identityFactory = new TaskIdentityFactory(project);
            identity = identityFactory.forNode(delegate);
            isInternalNode = identityFactory.wasInternalNode();
            isTaskInAnotherBuild = identityFactory.wasTaskInAnotherBuild();
        }
        return identity;
    }



    public String getDisplayName()
    {
        return getTask() != null ? getTask().getName() : delegate.toString();
    }



    @Nonnull
    public Class<?> getType()
    {
        Class<?> result = delegate.getClass();
        if (getTask() != null) {
            result = readType(getTask());
        }
        return result;
    }



    @Nonnull
    private Class<?> readType(@Nonnull final Task pTask)
    {
        Class<?> result = null;
        try {
            try {
                final TaskIdentity<?> taskIdentity = ((TaskInternal) pTask).getTaskIdentity();
                result = taskIdentity.type;
            }
            catch (NoSuchMethodError | ClassCastException e) {
                if (ReflectUtil.hasField(pTask, "publicType")) {
                    Class<?> publicType = (Class<?>) ReflectUtil.readField(pTask,
                        "type information", Collections.singletonList("publicType"));
                    result = publicType;
                }
            }
        }
        catch (RuntimeException | LinkageError e) {
            // fine, we'll use the fallback below
        }
        if (result == null) {
            result = pTask.getClass();
        }
        return result;
    }



    @Nonnull
    public List<TaskNodeHolder> getDependencySuccessors()
    {
        Iterable<?> dependencies = (Iterable<?>) ReflectUtil.callMethodIfPresent(delegate, "getDependencySuccessors");
        if (dependencies == null || !dependencies.iterator().hasNext()) {
            dependencies = (Iterable<?>) ReflectUtil.readField(delegate, "dependency successors",
                Arrays.asList("dependencySuccessors", "lifecycleSuccessors"), true);
        }
        if ((dependencies == null || !dependencies.iterator().hasNext()) && task != null) {
            dependencies = task.getTaskDependencies().getDependencies(task);
        }
        if (dependencies != null) {
            return wrapIntoList(dependencies);
        }
        return Collections.emptyList();
    }



    @Nonnull
    public List<TaskNodeHolder> getFinalizers()
    {
        // Only TaskNodes have the 'finalizers' field. The other forms of Nodes don't.
        Iterable<?> finalizerTasks = null;
        if (delegate instanceof Task) {
            Object td = ReflectUtil.callMethodIfPresent(delegate, "getFinalizedBy");
            if (td instanceof TaskDependency) {
                finalizerTasks = ((TaskDependency) td).getDependencies((Task) delegate);
            }
        }
        if ((finalizerTasks == null || !finalizerTasks.iterator().hasNext())
            && ReflectUtil.hasField(delegate, "finalizers"))
        {
            finalizerTasks = (Iterable<?>) ReflectUtil.readField(delegate, "finalizer tasks",
                Collections.singletonList("finalizers"));
        }
        if (finalizerTasks == null || !finalizerTasks.iterator().hasNext()) {
            finalizerTasks = (Iterable<?>) ReflectUtil.callMethodIfPresent(delegate, "getFinalizers");
        }
        if (finalizerTasks != null) {
            return wrapIntoList(finalizerTasks);
        }
        return Collections.emptyList();
    }



    @Nonnull
    private List<TaskNodeHolder> wrapIntoList(@Nonnull final Iterable<?> pNodes)
    {
        List<TaskNodeHolder> result = StreamSupport.stream(pNodes.spliterator(), false)
            .filter(TaskNodeHolder::hasRelevantNodeType)
            .map(node -> new TaskNodeHolder(project, node))
            .collect(Collectors.toList());
        return result;
    }



    @Nonnull
    public TaskInfoDto asTaskInfoDto(final boolean pFinalizer, final boolean pColorEnabled)
    {
        final TaskInfoDto result = new TaskInfoDto(getIdentity(), pFinalizer, getTask() != null, pColorEnabled);
        result.setName(getDisplayName());
        result.setGroup(getTask() != null ? getTask().getGroup() : null);
        result.setType(getType().getName());
        return result;
    }
}
