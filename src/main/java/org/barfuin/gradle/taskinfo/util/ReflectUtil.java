/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.gradle.api.GradleException;
import org.gradle.api.execution.TaskExecutionGraph;
import org.gradle.util.GradleVersion;


/**
 * Utility methods to access Gradle properties on objects reflectively.
 */
public final class ReflectUtil
{
    private ReflectUtil()
    {
        // utility class
    }



    @Nonnull
    public static Object readField(@Nonnull final Object pObject, final String pDescription,
        @Nonnull final List<String> pFieldNames)
    {
        return Objects.requireNonNull(readField(pObject, pDescription, pFieldNames, false));
    }



    @CheckForNull
    public static Object readField(@Nonnull final Object pObject, final String pDescription,
        @Nonnull final List<String> pFieldNames, final boolean pAllowNull)
    {
        final String objType = pObject.getClass().getName();
        for (String fieldName : pFieldNames) {
            try {
                Object result = FieldUtils.readField(pObject, fieldName, true);
                if (result != null) {
                    return result;
                }
                // we found the field, but it had no content (it was null)
                if (pAllowNull) {
                    return null;
                }
            }
            catch (IllegalArgumentException e) {
                // in commons-lang3, this means "field not found"
            }
            catch (IllegalAccessException | RuntimeException e) {
                throwGradleException(pDescription, objType, e);
            }
        }
        if (!pAllowNull) {
            throwGradleException(pDescription, objType, null);
        }
        return null;
    }



    public static boolean hasField(final Object pObject, final String pFieldName)
    {
        try {
            return FieldUtils.getField(pObject.getClass(), pFieldName, true) != null;
        }
        catch (RuntimeException e) {
            return false;
        }
    }



    @Nonnull
    public static Object getExecutionPlan(@Nonnull final TaskExecutionGraph pTaskGraph)
    {
        Object epResult = readField(pTaskGraph, "task execution plan",
            Arrays.asList("taskExecutionPlan", "executionPlan"));
        if (isInstanceOf("org.gradle.execution.plan.FinalizedExecutionPlan", epResult)) {
            epResult = ReflectUtil.callMethodIfPresent(epResult, "getContents");
            if (epResult == null) {
                throwGradleException("execution plan contents", "org.gradle.execution.plan.FinalizedExecutionPlan",
                    null);
            }
        }
        return epResult;
    }



    /**
     * Call the given method on the given object if it exists and return the result.
     *
     * @param pObject an object
     * @param pMethodName the name of a parameterless method
     * @param pParameters the parameters to pass to the called method, or <code>null</code> / left out for none
     * @return the method's return value. A result of <code>null</code> may mean that the method did not exist, or
     * its return value was <code>null</code>, or its return type was <code>void</code>.
     */
    @CheckForNull
    public static Object callMethodIfPresent(@Nullable final Object pObject, @Nonnull final String pMethodName,
        @Nullable final Object... pParameters)
    {
        Object result = null;
        if (pObject != null) {
            try {
                Class<?>[] paramTypes = null;
                if (pParameters != null) {
                    paramTypes = Arrays.stream(pParameters).sequential().map(Object::getClass).toArray(Class<?>[]::new);
                }
                Method method = MethodUtils.getAccessibleMethod(pObject.getClass(), pMethodName, paramTypes);
                if (method != null) {
                    result = method.invoke(pObject, pParameters);
                }
            }
            catch (IllegalAccessException | InvocationTargetException | RuntimeException e) {
                // ignore
            }
        }
        return result;
    }



    @CheckForNull
    public static Class<?> loadClassIfPossible(@Nonnull final String pFqcn)
    {
        Class<?> result = null;
        try {
            result = Class.forName(pFqcn);
        }
        catch (ClassNotFoundException e) {
            // ignore - this may happen due to different Gradle versions
        }
        return result;
    }



    /**
     * Tries to load the class determined by {@code pSuperFqcn}, and on success, determines if the given object
     * is an instance of it.
     *
     * @param pSuperFqcn fully qualified class name of the potential superclass
     * @param pObject any object
     * @return {@code true} if {@code pSuperFqcn} could be loaded and {@code pObject} is an instance of it;
     * {@code false} if either is not the case
     */
    public static boolean isInstanceOf(@Nonnull final String pSuperFqcn, @Nullable final Object pObject)
    {
        boolean result = false;
        if (pObject != null) {
            Class<?> superClass = loadClassIfPossible(pSuperFqcn);
            if (superClass != null) {
                result = superClass.isAssignableFrom(pObject.getClass());
            }
        }
        return result;
    }



    private static void throwGradleException(final String pDescription, final String pType, final Throwable pCause)
    {
        throw new GradleException("Failed to access " + pDescription + " in Gradle on object of type '"
            + pType + "'. When this happens, it normally means you are using a new version of Gradle which is not "
            + "supported by this plugin yet. You are using " + GradleVersion.current().toString()
            + ". Open an issue at https://gitlab.com/barfuin/gradle-taskinfo/-/issues.", pCause);
    }
}
