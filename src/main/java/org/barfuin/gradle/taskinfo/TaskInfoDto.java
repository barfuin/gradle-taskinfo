/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.barfuin.texttree.api.Node;
import org.barfuin.texttree.api.color.NodeColor;


/**
 * The information we collect on a task for use by this plugin. The <code>path</code> is the object identity, which is
 * also used for ordering.
 */
@JsonIgnoreProperties({"annotation", "color", "annotationColor", "text", "key", "children"})
public class TaskInfoDto
    implements Comparable<TaskInfoDto>, Node
{
    private static final TaskInfoSorting TASK_INFO_SORTING = new TaskInfoSorting();

    @JsonProperty
    private String name;

    @JsonProperty
    private String path;

    @JsonProperty
    private boolean finalizer;

    @JsonProperty
    private String group;

    @JsonProperty
    private String type;

    @JsonProperty
    private int queuePosition = -1;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final SortedSet<TaskInfoDto> dependencies = new TreeSet<>();

    @JsonIgnore
    private final boolean isTaskNode;

    @JsonIgnore
    private final boolean colored;



    public TaskInfoDto(final String pPath, final boolean pFinalizer, final boolean pIsTaskNode, final boolean pColored)
    {
        path = Objects.requireNonNull(pPath, "required argument pPath was null");
        finalizer = pFinalizer;
        isTaskNode = pIsTaskNode;
        colored = pColored;
    }



    public TaskInfoDto(final String pPath, final boolean pFinalizer)
    {
        this(pPath, pFinalizer, true, false);
    }



    /**
     * No-args constructor, which is intended only for JSON deserialization, not for use in a normal program.
     */
    public TaskInfoDto()
    {
        this("", false);
    }



    public String getName()
    {
        return name;
    }



    public void setName(final String pName)
    {
        name = pName;
    }



    @Nonnull
    public String getPath()
    {
        return path;
    }



    public void setPath(@Nonnull final String pPath)
    {
        path = Objects.requireNonNull(pPath, "required argument pPath was null");
    }



    boolean isTaskNode()
    {
        return isTaskNode;
    }



    @Override
    @CheckForNull
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public NodeColor getColor()
    {
        return isTaskNode || !colored ? null : NodeColor.DarkGray;
    }



    public boolean isFinalizer()
    {
        return finalizer;
    }



    public void setFinalizer(final boolean pFinalizer)
    {
        finalizer = pFinalizer;
    }



    public String getGroup()
    {
        return group;
    }



    public void setGroup(final String pGroup)
    {
        group = pGroup;
    }



    public String getType()
    {
        return type;
    }



    public void setType(final String pType)
    {
        type = pType;
    }



    public int getQueuePosition()
    {
        return queuePosition;
    }



    public void setQueuePosition(final int pQueuePosition)
    {
        queuePosition = pQueuePosition;
    }



    @Nonnull
    public SortedSet<TaskInfoDto> getDependencies()
    {
        return dependencies;
    }



    public void addDependency(final TaskInfoDto pNewDependency)
    {
        Objects.requireNonNull(pNewDependency, "argument pNewDependency was null");
        dependencies.add(pNewDependency);
    }



    @Override
    public boolean equals(final Object pOther)
    {
        if (this == pOther) {
            return true;
        }
        if (pOther == null || getClass() != pOther.getClass()) {
            return false;
        }
        return compareTo((TaskInfoDto) pOther) == 0;
    }



    @Override
    public int hashCode()
    {
        return Objects.hash(finalizer, path);
    }



    @Override
    public int compareTo(@Nonnull final TaskInfoDto pOther)
    {
        return TASK_INFO_SORTING.compare(this, pOther);
    }



    @Override
    public String getText()
    {
        return path + (finalizer ? " (finalizer)" : "");
    }



    @Override
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public String getKey()
    {
        return path;
    }



    @Override
    @CheckForNull
    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public String getAnnotation()
    {
        return '(' + getType() + ')';
    }



    @Nonnull
    @Override
    public SortedSet<TaskInfoDto> getChildren()
    {
        return dependencies;
    }
}
