/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.util.ReflectUtil;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.gradle.api.execution.TaskExecutionGraph;


/**
 * Determines which task we start our analysis on.
 */
public class EntryNodeProvider
{
    private final Project project;

    private final TaskExecutionGraph taskGraph;

    private final SortedSet<String> entryNodePaths = new TreeSet<>();

    private final List<TaskNodeHolder> entryNodes = new ArrayList<>();

    private String invokedTask = null;



    public EntryNodeProvider(@Nonnull final Project pProject, @Nonnull final TaskExecutionGraph pTaskGraph)
    {
        project = Objects.requireNonNull(pProject, "pProject was null");
        taskGraph = Objects.requireNonNull(pTaskGraph, "pTaskGraph was null");
        analyzeTaskGraph();
    }



    private void analyzeTaskGraph()
    {
        final Object executionPlan = ReflectUtil.getExecutionPlan(taskGraph);
        final Iterable<?> rawEntryNodes = (Iterable<?>)
            ReflectUtil.readField(executionPlan, "list of invoked tasks", Arrays.asList("entryNodes", "entryTasks"));
        invokedTask = readInvokedTaskname(rawEntryNodes);
        processEntryNodes(rawEntryNodes);
    }



    private void processEntryNodes(@Nonnull final Iterable<?> pEntryNodes)
    {
        boolean taskInfoInvoked = false;
        for (Object realNode : pEntryNodes) {
            TaskNodeHolder holder = new TaskNodeHolder(project, realNode);
            if (holder.isOneOfOurTasks()) {
                taskInfoInvoked = true;
            }
            else {
                entryNodes.add(holder);
                entryNodePaths.add(holder.getIdentity());
            }
        }

        if (entryNodes.isEmpty()) {
            throw new GradleException("No task specified for " + invokedTask + ".");
        }
        if (taskInfoInvoked && entryNodes.size() > 1
            && !GradleTaskInfoPlugin.TASKINFO_ORDERED_TASK_NAME.equals(invokedTask))
        {
            project.getLogger().warn("WARNING: More than one task specified as argument of " + invokedTask
                + " " + entryNodePaths + ". Will report on '" + entryNodes.get(0).getIdentity() + "'.");
        }
    }



    @Nonnull
    private String readInvokedTaskname(@Nonnull final Iterable<?> pEntryNodes)
    {
        SortedSet<String> candidates = new TreeSet<>();
        for (Object realNode : pEntryNodes) {
            TaskNodeHolder holder = new TaskNodeHolder(project, realNode);
            if (holder.isOneOfOurTasks()) {
                candidates.add(holder.getDisplayName());
            }
        }
        return candidates.size() == 1 ? candidates.first() : "task info";
    }



    @Nonnull
    public TaskNodeHolder getEffectiveEntryNode()
    {
        return entryNodes.get(0);
    }



    @Nonnull
    public String getLabel()
    {
        return String.join(", ", entryNodePaths);
    }
}
