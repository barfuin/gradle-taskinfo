/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import org.fusesource.jansi.Ansi;


/**
 * Wraps a StringBuilder with optional color support.
 */
public class ColorizableAppender
{
    private final StringBuilder sb = new StringBuilder();

    private final Ansi ansi = Ansi.ansi(sb);

    private final boolean colorized;



    public ColorizableAppender(final boolean pColorized)
    {
        colorized = pColorized;
    }



    public void append(final char pChar)
    {
        if (colorized) {
            ansi.a(pChar);
        }
        else {
            sb.append(pChar);
        }
    }



    public void append(final String pText)
    {
        if (colorized) {
            ansi.a(pText);
        }
        else {
            sb.append(pText);
        }
    }



    public void newline()
    {
        if (colorized) {
            ansi.newline();
        }
        else {
            sb.append(System.lineSeparator());
        }
    }



    public void colorNumbers()
    {
        if (colorized) {
            ansi.fgYellow();
        }
    }



    public void colorAnnotations()
    {
        if (colorized) {
            ansi.fgBrightBlack();
        }
    }



    public void colorReset()
    {
        if (colorized) {
            ansi.reset();
        }
    }



    public String toString()
    {
        if (colorized) {
            return ansi.toString();
        }
        return sb.toString();
    }
}
