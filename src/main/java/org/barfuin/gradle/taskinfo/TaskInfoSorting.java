/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.gradle.api.Project;


/**
 * Comparator that defines the natural ordering of our {@link TaskInfoDto} objects.
 */
public final class TaskInfoSorting
    implements Comparator<TaskInfoDto>, Serializable
{
    private static final long serialVersionUID = 0L;



    @Override
    public int compare(final TaskInfoDto pObject1, final TaskInfoDto pObject2)
    {
        int result = 0;
        if (pObject1 == null) {
            if (pObject2 != null) {
                result = 1;
            }
        }
        else {
            if (pObject2 == null) {
                result = -1;
            }
            else {
                List<String> path1 = split(pObject1.getPath());
                List<String> path2 = split(pObject2.getPath());
                result = Integer.compare(path1.size(), path2.size());
                if (result == 0) {
                    for (int i = 0; i < path1.size(); i++) {
                        if (i == path1.size() - 1) {
                            result = Boolean.compare(pObject1.isFinalizer(), pObject2.isFinalizer());
                        }
                        if (result == 0) {
                            result = path1.get(i).compareTo(path2.get(i));
                        }
                        if (result != 0) {
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }



    private List<String> split(final String pPath)
    {
        String[] fragments = pPath.split(Project.PATH_SEPARATOR);
        return Arrays.stream(fragments).filter(e -> !e.isEmpty()).collect(Collectors.toList());
    }
}
