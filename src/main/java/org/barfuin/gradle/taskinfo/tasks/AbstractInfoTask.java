/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.tasks;

import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.GradleTaskInfoPlugin;
import org.barfuin.gradle.taskinfo.TaskInfoExtension;
import org.barfuin.gradle.taskinfo.util.GradleVersionUtil;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.fusesource.jansi.Ansi;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.gradle.api.tasks.Internal;


/**
 * Common superclass of all tasks provided by this plugin
 */
public abstract class AbstractInfoTask
    extends DefaultTask
{
    private TaskNodeHolder entryNode = null;

    private String entryNodeLabel = "<unknown>";



    protected AbstractInfoTask()
    {
        super();
        getOutputs().upToDateWhen(task -> false);   // always run when invoked
    }



    public abstract void executeTaskInfo();  // just "execute" was already taken in Gradle < 5



    /**
     * Validate that preconditions are met. This is necessary because in Gradle 4.10.2, the bug gradle/gradle#6900
     * causes exceptions thrown from the takGraph.whenReady() listener to be swallowed. Thus, we re-check here and
     * throw the exception again during the execution phase if necessary.
     */
    protected void validatePreconditions()
    {
        GradleVersionUtil.validateMinimumGradleVersion(getName());
        if (entryNode == null) {
            throw new GradleException("No task specified for " + GradleTaskInfoPlugin.TASKINFO_TASK_NAME + ".");
        }
    }



    @Nonnull
    @Internal
    protected TaskInfoExtension getConfig()
    {
        final Project project = getProject();
        TaskInfoExtension result = project.getExtensions().getByType(TaskInfoExtension.class);
        if (project.hasProperty("taskinfo.clipped")) {
            result.setClipped(Boolean.parseBoolean(project.getProperties().get("taskinfo.clipped").toString()));
        }
        if (project.hasProperty("taskinfo.color")) {
            result.setColor(Boolean.parseBoolean(project.getProperties().get("taskinfo.color").toString()));
        }
        if (project.hasProperty("taskinfo.showTaskTypes")) {
            result.setShowTaskTypes(Boolean.parseBoolean(
                project.getProperties().get("taskinfo.showTaskTypes").toString()));
        }
        if (project.hasProperty("taskinfo.internal")) {
            result.setInternal(Boolean.parseBoolean(project.getProperties().get("taskinfo.internal").toString()));
        }
        return result;
    }



    public void setEntryNode(final TaskNodeHolder pEntryNode)
    {
        entryNode = pEntryNode;
    }



    @Internal
    protected TaskNodeHolder getEntryNode()
    {
        return entryNode;
    }



    public void setEntryNodeLabel(final String pEntryNodeLabel)
    {
        entryNodeLabel = pEntryNodeLabel != null ? pEntryNodeLabel : "<unknown>";
    }



    @Nonnull
    @Internal
    public String getEntryNodeLabel()
    {
        return entryNodeLabel;
    }



    protected void printHintOnInternalNodes(@Nonnull final String pSubject, final boolean pIsColor)
    {
        String msg;
        if (pIsColor) {
            msg = Ansi.ansi().a(pSubject + " in ").fgBrightBlack().a("gray").reset().a(" are not backed by tasks "
                + "but by internal nodes added by Gradle.").toString();
        }
        else {
            msg = "The above includes internal nodes added by Gradle which are not backed by tasks.";
        }
        getLogger().lifecycle(msg);
    }
}
