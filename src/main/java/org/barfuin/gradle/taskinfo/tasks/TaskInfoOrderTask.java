/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.tasks;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.barfuin.gradle.taskinfo.ColorizableAppender;
import org.barfuin.gradle.taskinfo.TaskInfoExtension;
import org.barfuin.gradle.taskinfo.TaskProbe;
import org.barfuin.gradle.taskinfo.util.TaskNodeHolder;
import org.gradle.api.tasks.TaskAction;


/**
 * Task which gathers the required task info and shows it as the execution queue.
 */
public class TaskInfoOrderTask
    extends AbstractInfoTask
{
    public TaskInfoOrderTask()
    {
        super();
        setDescription("Displays which tasks would be executed in what order for a given task, and their task types.");
    }



    @Override
    @TaskAction
    public void executeTaskInfo()
    {
        validatePreconditions();
        final TaskInfoExtension config = getConfig();

        final TaskProbe taskProbe = new TaskProbe(getProject(), config);
        List<TaskNodeHolder> taskInfos = taskProbe.buildOrder();

        outputTaskInfo(taskInfos, config);
        if (taskProbe.isNonTaskNodesPresent() && config.isInternal()) {
            printHintOnInternalNodes("Items", config.isColor());
        }
        if (!getProject().getGradle().getIncludedBuilds().isEmpty()) {
            String msg = "The above may be missing information on tasks from included builds. The tiTree task may have "
                + "a little more information. You are seeing this because taskinfo.disableSafeguard=true.";
            getLogger().lifecycle(msg);
        }
    }



    private void outputTaskInfo(final List<TaskNodeHolder> pTaskInfos, final TaskInfoExtension pConfig)
    {
        String output = renderTaskInfo(pTaskInfos, pConfig);
        getLogger().lifecycle(output);
    }



    private String renderTaskInfo(final List<TaskNodeHolder> pTaskInfos, final TaskInfoExtension pConfig)
    {
        final int m = getLengthOfLongestTaskName(pTaskInfos);
        final ColorizableAppender appender = new ColorizableAppender(pConfig.isColor());
        appender.newline();
        appender.append("In order to execute [");
        appender.append(getEntryNodeLabel());
        appender.append("], the following tasks would be executed in this order:");
        appender.newline();
        appender.newline();

        for (int i = 0; i < pTaskInfos.size(); i++) {
            final TaskNodeHolder taskInfo = pTaskInfos.get(i);
            final String path = taskInfo.getIdentity();

            appender.colorNumbers();
            appender.append(String.format("%3d", i + 1));
            appender.append('.');
            appender.colorReset();

            appender.append(' ');
            if (taskInfo.getTask() == null) {
                appender.colorAnnotations();
            }
            appender.append(path);
            if (taskInfo.getTask() == null) {
                appender.colorReset();
            }
            // finalizer flag is meaningless for ordered output

            if (pConfig.isShowTaskTypes()) {
                appender.append(StringUtils.repeat(' ', m - path.length() + 1));
                appender.colorAnnotations();
                appender.append('(');
                appender.append(taskInfo.getType().getName());
                appender.append(')');
                appender.colorReset();
            }
            appender.newline();
        }
        return appender.toString();
    }



    private int getLengthOfLongestTaskName(final List<TaskNodeHolder> pTaskInfos)
    {
        return Collections.max(pTaskInfos, Comparator.comparing(dto -> dto.getIdentity().length()))
            .getIdentity().length();
    }
}
