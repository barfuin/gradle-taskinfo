/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Assertions;


/**
 * Helper class for transforming output from the component under test.
 */
public final class LogXformUtil
{
    /** <img src="doc-files/LogXformUtil-1.png" /> */
    public static final Pattern ANSI_COLOR_ESCAPES_PATTERN = Pattern.compile("\\x1b\\[[0-9;]*m");

    /** <img src="doc-files/LogXformUtil-2.png" /> */
    private static final Pattern ADDRESS_PATTERN =
        Pattern.compile("(?:workNodeAction|transformationStep|resolveMutations|unknownNode)([0-9a-f]+)[ \\x1b\")]");



    private LogXformUtil()
    {
        // utility class
    }



    public static String sanitizeLogAnsi(final String pActualLog)
    {
        String result = unixifyLineBreaks(pActualLog);
        return normalizeAddresses(result);
    }



    public static String sanitizeLog(final String pActualLog)
    {
        String result = sanitizeLogAnsi(pActualLog);
        return removeAnsiEscapes(result);
    }



    public static String unixifyLineBreaks(final String pLines)
    {
        Assertions.assertNotNull(pLines);
        return pLines.replaceAll(Pattern.quote("\r\n") + '?', Matcher.quoteReplacement("\n"));
    }



    public static String normalizeAddresses(final String pLines)
    {
        Assertions.assertNotNull(pLines);
        final String mask = "_xxxxxxx";
        final String ansiReset = "\u001b[m";

        boolean found = false;
        String result = pLines;
        do {
            Matcher matcher = ADDRESS_PATTERN.matcher(result);
            found = matcher.find();
            if (found) {
                boolean hasEsc = result.charAt(matcher.end(1)) == ansiReset.charAt(0);
                boolean isJson = result.charAt(matcher.end(1)) == '"';
                int maskEnd = matcher.start(1) + mask.length();

                if (isJson) {
                    result = result.substring(0, matcher.start(1)) + mask + result.substring(matcher.end(1));
                }
                else {
                    result = result.substring(0, matcher.start(1)) + mask + result.substring(maskEnd);
                }

                if (hasEsc) {
                    result = result.substring(0, maskEnd) + ansiReset + result.substring(maskEnd + ansiReset.length());
                }
            }
        } while (found);
        return result;
    }



    public static String removeAnsiEscapes(final String pLines)
    {
        Assertions.assertNotNull(pLines);
        return ANSI_COLOR_ESCAPES_PATTERN.matcher(pLines).replaceAll("");
    }
}
