/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import org.barfuin.gradle.taskinfo.test.LogXformUtil;
import org.barfuin.gradle.taskinfo.test.TestIoUtil;
import org.gradle.util.GradleVersion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


/**
 * Functional tests of the gradle-taskinfo plugin.
 */
public class GradleTaskInfoPluginFunctionalTest
{
    /** The first version of Gradle that could perform deferred configuration */
    private static final GradleVersion GRADLE_49 = GradleVersion.version("4.9-rc-1");

    /**
     * The first version of Gradle that knew about artifact transforms and
     * {@link org.gradle.api.artifacts.transform.InputArtifact @InputArtifact} annotations
     */
    private static final GradleVersion GRADLE_53 = GradleVersion.version("5.3");

    /** The first version of Gradle where TransformationNodes have an "owning project" */
    private static final GradleVersion GRADLE_60 = GradleVersion.version("6.0.0-rc-1");

    @TempDir
    private File testProjectDir;

    private Fixtures fixtures = null;



    /**
     * The versions of Gradle for which the test cases in this suite will be executed.
     *
     * @return a stream of Gradle versions to test
     */
    static Stream<String> gradleVersions()
    {
        return Stream.of(
            "3.4",
            "3.5.1",
            "4.10.3",
            "5.6.4",
            "6.9.4",
            "7.5.1",
            "7.6.3",
            "8.5"
        );
    }



    static Stream<Arguments> gradleVersionsBool()
    {
        final List<Arguments> result = new ArrayList<>();
        gradleVersions().forEach(gradleVersion -> {
            result.add(Arguments.of(gradleVersion, true));
            result.add(Arguments.of(gradleVersion, false));
        });
        return result.stream();
    }



    static Stream<Arguments> gradleVersionsBoolBool()
    {
        final List<Arguments> result = new ArrayList<>();
        gradleVersions().forEach(gradleVersion -> {
            result.add(Arguments.of(gradleVersion, true, true));
            result.add(Arguments.of(gradleVersion, true, false));
            result.add(Arguments.of(gradleVersion, false, true));
            result.add(Arguments.of(gradleVersion, false, false));
        });
        return result.stream();
    }



    static Stream<Arguments> gradleVersionsTasksBool()
    {
        final List<String> tasks = Arrays.asList("tiTree", "tiOrder", "tiJson");
        final List<Arguments> result = new ArrayList<>();
        gradleVersions().forEach(gradleVersion ->
            tasks.forEach(task -> {
                result.add(Arguments.of(gradleVersion, task, true));
                result.add(Arguments.of(gradleVersion, task, false));
            })
        );
        return result.stream();
    }



    // CHECK JUnit5 can't easily parameterize @BeforeEach methods. So we call it explicitly every time.
    // @BeforeEach
    public void beforeTest(final String pGradleVersion)
    {
        fixtures = new Fixtures(testProjectDir, pGradleVersion);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "canRunUnrelatedTask(): Gradle {0}, unrelated1, setup1")
    @DisplayName("An unrelated task can be invoked normally even when the plugin is applied")
    public void canRunUnrelatedTask(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "unrelated1");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains("Hello world from the unrelated task 1."));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "canRunUnrelatedTask2(): Gradle {0}, unrelated, setup1")
    @DisplayName("More than one unrelated tasks can be invoked normally even with the plugin applied")
    public void canRunUnrelatedTask2(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "unrelated1", "unrelated2");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains("Hello world from the unrelated task 1."));
        Assertions.assertTrue(buildLog.contains("Hello world from the unrelated task 2."));
        Assertions.assertFalse(
            buildLog.contains("WARNING: More than one task specified as argument of task info [:unrelated"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "simpleTest(): Gradle {0}, tiTree, setup1")
    @DisplayName("Invoke tiTree on a simple sunny day scenario")
    public void simpleTest(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, "expected/setup1/tiTree.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "simpleTestReverseInput(): Gradle {0}, tiTree, setup1")
    @DisplayName("Invoke tiTree on a simple sunny day scenario with reversed arguments")
    public void simpleTestReverseInput(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "taskA", "tiTree");   // tiTree at end
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, "expected/setup1/tiTree.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testJsonOutput(): Gradle {0}, tiJson, setup1")
    @DisplayName("Invoke tiJson on a simple sunny day scenario")
    public void testJsonOutput(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        fixtures.act(true, "tiJson", "taskA");

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiJson.json"));
        final String actual = TestIoUtil.readFileAndClose(
            new FileInputStream(new File(testProjectDir, "build/taskinfo/taskinfo-taskA.json")));
        Assertions.assertEquals(expected, actual);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testJsonOutputReverseInput(): Gradle {0}, tiJson, setup1")
    @DisplayName("Invoke tiJson on a simple sunny day scenario with reversed arguments")
    public void testJsonOutputReverseInput(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        fixtures.act(true, "taskA", "tiJson");   // tiJson at end

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiJson.json"));
        final String actual = TestIoUtil.readFileAndClose(
            new FileInputStream(new File(testProjectDir, "build/taskinfo/taskinfo-taskA.json")));
        Assertions.assertEquals(expected, actual);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testOrderedOutput(): Gradle {0}, tiOrder, setup1")
    @DisplayName("Invoke tiOrder on a simple sunny day scenario")
    public void testOrderedOutput(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiOrder", "taskA");
        fixtures.assertAnsiEscape(buildLog, true);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiOrder.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    /**
     * Test that at least one entry node is given other than our own tasks. This means also that it is not currently
     * possible to run tiTree on a tiTree task.
     *
     * @param pGradleVersion the version of Gradle to use for the test
     * @throws IOException failed to set up test case
     */
    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testMissingEntryNode(): Gradle {0}, tiTree, setup1")
    @DisplayName("Trying to run tiTree on itself should fail")
    public void testMissingEntryNode(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(false, "tiTree", "tiTree");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains("GradleException: No task specified for tiTree"));
    }



    /**
     * Test that when more than one possible entry node is specified, we use the first.
     *
     * @param pGradleVersion the version of Gradle to use for the test
     * @throws IOException failed to set up test case
     */
    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testMultipleEntryNodes(): Gradle {0}, tiTree, setup1")
    @DisplayName("Only the first of multiple entry nodes will be reported on")
    public void testMultipleEntryNodes(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiTree", "taskB", "taskE");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        Assertions.assertTrue(buildLog.contains(
            "WARNING: More than one task specified as argument of tiTree [:taskB, :taskE]. Will report on ':taskB'."));
        Assertions.assertTrue(buildLog.contains("\n:taskB (org.gradle.api.DefaultTask)\n"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testOrderedNoDependencies(): Gradle {0}, tiOrder, setup1")
    @DisplayName("Invoking tiOrder on a task that has no dependencies should yield only this task")
    public void testOrderedNoDependencies(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "tiOrder", "taskB");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains("1. :taskB (org.gradle.api.DefaultTask)\n\n"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testColorTurnedOffOrdered(): Gradle {0}, tiOrder, setup1")
    @DisplayName("Invokes tiOrder in a simple scenario without coloring")
    public void testColorTurnedOffOrdered(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "-Ptaskinfo.color=false", "tiOrder", "taskA");

        fixtures.assertAnsiEscape(buildLog, false);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiOrder.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersionsBoolBool")
    @ParameterizedTest(name = "testSetup2Combinations(): Gradle {0}, tiTree, setup2, color={1}, clipped={2}")
    @DisplayName("Invoke tiTree on a task with more complex, partially repeating dependencies")
    public void testSetup2Combinations(final String pGradleVersion, final boolean pColor, final boolean pClipped)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "-Ptaskinfo.color=" + pColor, "-Ptaskinfo.clipped=" + pClipped,
            "tiTree", "taskA");

        fixtures.assertAnsiEscape(buildLog, pColor);
        buildLog = LogXformUtil.sanitizeLogAnsi(buildLog);

        final String expectedFilename =
            "expected/setup2/tiTree" + (pColor ? "-color" : "") + (pClipped ? "-clipped" : "") + ".txt";
        final String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, expectedFilename));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertFalse(buildLog.contains("The above includes at least one task from another build."));
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testCyclicDependencyDetection(): Gradle {0}, tiTree, setup3")
    @DisplayName("Test that cycle protection works for tiTree")
    public void testCyclicDependencyDetection(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup3");

        String buildLog = fixtures.act(true, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, "expected/setup3/tiTree.txt"));
        Assertions.assertTrue(buildLog.contains(expected));
        fixtures.assertNoWarnings(buildLog);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testMultiTaskOrdered(): Gradle {0}, tiOrder, setup2")
    @DisplayName("The tiOrder task accepts multiple entry nodes")
    public void testMultiTaskOrdered(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "tiOrder", "taskB", "taskD");   // multiple args OK for tiOrder
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup2/tiOrder.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
        Assertions.assertFalse(buildLog.contains("missing information on tasks from included builds"));
        Assertions.assertFalse(buildLog.contains("WARNING: More than one task"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testMultiTaskOrderedReversed(): Gradle {0}, tiOrder, setup2")
    @DisplayName("The tiOrder task accepts multiple entry nodes, with reversed arguments")
    public void testMultiTaskOrderedReversed(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "taskB", "taskD", "tiOrder");   // tiOrder at end
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup2/tiOrder.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
        Assertions.assertFalse(buildLog.contains("WARNING: More than one task"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testMultiTaskOrderedJumbled(): Gradle {0}, tiOrder, setup2")
    @DisplayName("The tiOrder task accepts multiple entry nodes, with jumbled arguments")
    public void testMultiTaskOrderedJumbled(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup2");

        String buildLog = fixtures.act(true, "taskB", "tiOrder", "taskD");   // tiOrder in middle position
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup2/tiOrder.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
        Assertions.assertFalse(buildLog.contains("WARNING: More than one task"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testMultiProjectBuildTaskMatching(): Gradle {0}, tiTree, setup4")
    @DisplayName("Tests that a warning is issued when a single entry node matches to more than one actual node")
    public void testMultiProjectBuildTaskMatching(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup4");

        String buildLog = fixtures.act(true, "tiTree", "taskA");   // matches 3 tasks in reality!
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, "expected/setup4/tiTree.txt"));
        Assertions.assertTrue(buildLog.contains(expected));
        Assertions.assertTrue(buildLog.contains("WARNING: More than one task"));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testDeferredConfiguration(): Gradle {0}, tiTree, setup5")
    @DisplayName("Tests that the tiTree task works in projects that use deferred configuration")
    public void testDeferredConfiguration(final String pGradleVersion)
        throws IOException
    {
        Assumptions.assumeTrue(GradleVersion.version(pGradleVersion).compareTo(GRADLE_49) >= 0);

        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup5");

        String buildLog = fixtures.act(true, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = ":taskA           (org.gradle.api.DefaultTask)\n"
            + "+--- :taskB      (org.gradle.api.DefaultTask)\n"
            + "|    `--- :taskC (org.gradle.api.DefaultTask)\n"
            + "`--- :taskD      (org.gradle.api.DefaultTask)\n"
            + "\n";
        Assertions.assertTrue(buildLog.contains(expected));
        fixtures.assertNoWarnings(buildLog);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testDeferredConfigurationOrder(): Gradle {0}, tiOrder, setup5")
    @DisplayName("Tests that the tiOrder task works in projects that use deferred configuration")
    public void testDeferredConfigurationOrder(final String pGradleVersion)
        throws IOException
    {
        Assumptions.assumeTrue(GradleVersion.version(pGradleVersion).compareTo(GRADLE_49) >= 0);

        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup5");

        String buildLog = fixtures.act(true, "tiOrder", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected =
            "In order to execute [:taskA], the following tasks would be executed in this order:\n"
                + "\n"
                + "  1. :taskC (org.gradle.api.DefaultTask)\n"
                + "  2. :taskB (org.gradle.api.DefaultTask)\n"
                + "  3. :taskD (org.gradle.api.DefaultTask)\n"
                + "  4. :taskA (org.gradle.api.DefaultTask)\n"
                + "\n";
        Assertions.assertTrue(buildLog.contains(expected));
        fixtures.assertNoWarnings(buildLog);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testTypesOmittedOrder(): Gradle {0}, tiOrder, setup1")
    @DisplayName("Tests that display of task types can be disabled for tiOrder")
    public void testTypesOmittedOrder(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "-Ptaskinfo.showTaskTypes=false", "tiOrder", "taskA");
        fixtures.assertAnsiEscape(buildLog, true);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiOrder-noTypes.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testTypesOmittedTree(): Gradle {0}, tiTree, setup1")
    @DisplayName("Tests that display of task types can be disabled for tiTree")
    public void testTypesOmittedTree(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        String buildLog = fixtures.act(true, "-Ptaskinfo.showTaskTypes=false", "tiTree", "taskA");
        fixtures.assertAnsiEscape(buildLog, true);
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiTree-noTypes.txt"));
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testTypesOmittedJson(): Gradle {0}, tiJson, setup1")
    @DisplayName("Tests that disabling task types has no effect on the tiJson output")
    public void testTypesOmittedJson(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup1");

        // tiJson must be unaffected by this option!
        fixtures.act(true, "-Ptaskinfo.showTaskTypes=false", "tiJson", "taskA");

        final String expected = TestIoUtil.readFileAndClose(
            getStreamFor(pGradleVersion, "expected/setup1/tiJson.json"));
        final String actual = TestIoUtil.readFileAndClose(
            new FileInputStream(new File(testProjectDir, "build/taskinfo/taskinfo-taskA.json")));
        Assertions.assertEquals(expected, actual);
    }



    // @Disabled
    @MethodSource("gradleVersionsBoolBool")
    @ParameterizedTest(name = "testTransformNodesTree(): Gradle {0}, tiTree, setup6, internalNodes={1}, color={2}")
    @DisplayName("Invoke tiTree on a project with transformation nodes")
    public void testTransformNodesTree(final String pGradleVersion, final boolean pInternal, final boolean pColor)
        throws IOException
    {
        Assumptions.assumeTrue(GradleVersion.version(pGradleVersion).compareTo(GRADLE_53) >= 0);

        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup6-transform");

        String buildLog = fixtures.act(true, "-Ptaskinfo.internal=" + pInternal, "-Ptaskinfo.color=" + pColor,
            "tiTree", ":test");
        buildLog = LogXformUtil.sanitizeLogAnsi(buildLog);

        final String expectedFilename =
            "expected/setup6/tiTree" + (pInternal ? "-internal" : "") + (pColor ? "-color" : "") + ".txt";
        String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, expectedFilename));
        if (GradleVersion.version(pGradleVersion).compareTo(GRADLE_60) < 0) {
            expected = expected.replace(":transformationStep_xxxxxxx", "transformationStep_xxxxxxx ");
        }
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertFalse(buildLog.contains("The above includes at least one task from another build."));
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersionsBoolBool")
    @ParameterizedTest(name = "testTransformNodesOrder(): Gradle {0}, tiOrder, setup6, internalNodes={1}, color={2}")
    @DisplayName("Invoke tiOrder on a project with transformation nodes")
    public void testTransformNodesOrder(final String pGradleVersion, final boolean pInternal,
        final boolean pColor)
        throws IOException
    {
        Assumptions.assumeTrue(GradleVersion.version(pGradleVersion).compareTo(GRADLE_53) >= 0);

        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup6-transform");

        String buildLog = fixtures.act(true, "-Ptaskinfo.internal=" + pInternal, "-Ptaskinfo.color=" + pColor,
            "tiOrder", "test");
        buildLog = LogXformUtil.sanitizeLogAnsi(buildLog);

        final String expectedFilename =
            "expected/setup6/tiOrder" + (pInternal ? "-internal" : "") + (pColor ? "-color" : "") + ".txt";
        String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, expectedFilename));
        if (GradleVersion.version(pGradleVersion).compareTo(GRADLE_60) < 0) {
            final String ansiReset = "\u001b[m";
            expected = expected.replace(":transformationStep_xxxxxxx" + (pColor ? ansiReset : ""),
                "transformationStep_xxxxxxx" + (pColor ? ansiReset : "") + " ");
        }
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertFalse(buildLog.contains("missing information on tasks from included builds"));
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersionsBool")
    @ParameterizedTest(name = "testTransformNodesJson(): Gradle {0}, tiJson, setup6, internalNodes={1}")
    @DisplayName("Invoke tiJson on a project with transformation nodes")
    public void testTransformNodesJson(final String pGradleVersion, final boolean pInternal)
        throws IOException
    {
        Assumptions.assumeTrue(GradleVersion.version(pGradleVersion).compareTo(GRADLE_53) >= 0);

        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup6-transform");

        fixtures.act(true, "-Ptaskinfo.internal=" + pInternal, "tiJson", "test");

        final String actual = LogXformUtil.normalizeAddresses(TestIoUtil.readFileAndClose(
            new FileInputStream(new File(testProjectDir, "build/taskinfo/taskinfo-test.json"))));

        final String expectedFilename = "expected/setup6/tiJson" + (pInternal ? "-internal" : "") + ".json";
        String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, expectedFilename));
        if (GradleVersion.version(pGradleVersion).compareTo(GRADLE_60) < 0) {
            expected = expected.replace(":transformationStep_xxxxxxx", "transformationStep_xxxxxxx");
        }
        Assertions.assertEquals(expected, actual);
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testExtendedJson(): Gradle {0}, tiJson, setup7")
    @DisplayName("Tests that users can react to tiJson output with custom code")
    public void testExtendedJson(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup7-extendedJson");

        String buildLog = fixtures.act(true, "tiJson", "compileJava");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        final String expected = "==> Task ':compileJava' has declared inputs. <==\n";
        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains(expected));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testIncludedBuild(): Gradle {0}, tiTree, setup8")
    @DisplayName("Tests that the plugin fails for composite builds with a useful error message")
    public void testIncludedBuild(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup8-composite");

        String buildLog = fixtures.act(false, "tiTree", "taskA");
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertTrue(buildLog.contains("does not support composite builds at this time.\nYou can still"));
        Assertions.assertFalse(buildLog.contains("Hello from "));
    }



    // @Disabled
    @MethodSource("gradleVersions")
    @ParameterizedTest(name = "testIncludedBuildNormalExecutionWithoutSafeguard(): Gradle {0}, taskA, setup8")
    @DisplayName("Tests that the composite build safeguard can be disabled")
    public void testIncludedBuildNormalExecutionWithoutSafeguard(final String pGradleVersion)
        throws IOException
    {
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup8-composite");
        TestIoUtil.string2File("taskinfo.disableSafeguard = true\n",
            new File(testProjectDir, "gradle.properties").getAbsolutePath());

        String buildLog = fixtures.act(true, "taskA");  // no gradle-taskinfo task, just a regular composite build
        buildLog = LogXformUtil.sanitizeLog(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertFalse(buildLog.contains("does not support composite builds at this time"));
        Assertions.assertTrue(buildLog.contains("Hello from includedA"));
        Assertions.assertTrue(buildLog.contains("Hello from includedB"));
    }



    // @Disabled
    @MethodSource("gradleVersionsTasksBool")
    @ParameterizedTest(name = "testIncludedBuildWithoutSafeguard(): Gradle {0}, {1}, setup8, color={2}")
    @DisplayName("Test output on composite builds with safeguard disabled (partial)")
    public void testIncludedBuildWithoutSafeguard(final String pGradleVersion, final String pTaskName,
        final boolean pColor)
        throws IOException
    {
        final boolean isJson = "tiJson".equals(pTaskName);
        beforeTest(pGradleVersion);
        fixtures.arrangeTestProject("setup8-composite");

        String buildLog = fixtures.act(true, "-Ptaskinfo.disableSafeguard=true", "-Ptaskinfo.color=" + pColor,
            pTaskName, "taskA");
        buildLog = LogXformUtil.sanitizeLogAnsi(buildLog);

        fixtures.assertNoWarnings(buildLog);
        Assertions.assertFalse(buildLog.contains("does not support composite builds at this time"));

        final String expectedFilename = "expected/setup8/" + pTaskName + (pColor && !isJson ? "-color" : "")
            + (isJson ? ".json" : ".txt");
        String expected = TestIoUtil.readFileAndClose(getStreamFor(pGradleVersion, expectedFilename));
        if (isJson) {
            final String actual = TestIoUtil.readFileAndClose(
                new FileInputStream(new File(testProjectDir, "build/taskinfo/taskinfo-taskA.json")));
            Assertions.assertEquals(expected, actual);
        }
        else {
            Assertions.assertTrue(buildLog.contains(expected));
        }
    }



    /**
     * Finds the given file and returns an open stream to it. The given Gradle version is appended to the file name
     * just before the extension. If that more specific file exists, it is used. Otherwise, the given file name is
     * used. This is to compensate for changed Gradle output in that version.
     *
     * @param pGradleVersion the current version of Gradle being tested
     * @param pFilename the original file name
     * @return the effective input stream
     */
    @CheckForNull
    private InputStream getStreamFor(final String pGradleVersion, final String pFilename)
    {
        final String specificFilename = insertBeforeLastDot(pFilename, "-" + pGradleVersion);
        InputStream result = getClass().getResourceAsStream(specificFilename);
        if (result == null) {
            // If the specific file does not exist, use the unmodified filename.
            result = getClass().getResourceAsStream(pFilename);
        }
        return result;
    }



    @Nonnull
    private String insertBeforeLastDot(@Nonnull final String pString, @Nonnull final String pInsert)
    {
        int dotPos = pString.indexOf('.');
        Assertions.assertTrue(dotPos > 0);
        return pString.substring(0, dotPos) + pInsert + pString.substring(dotPos);
    }
}
