/*
 * Copyright 2020-2024 The gradle-taskinfo contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.taskinfo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

import org.apache.commons.io.FileUtils;
import org.barfuin.gradle.taskinfo.test.LogXformUtil;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.jupiter.api.Assertions;


/**
 * Utility class providing test fixtures for the functional tests.
 */
public class Fixtures
{
    private final File projectDir;

    private final String gradleVersion;



    public Fixtures(@Nonnull final File pProjectDir, @Nonnull final String pGradleVersion)
    {
        Assertions.assertNotNull(pProjectDir);
        Assertions.assertNotNull(pGradleVersion);
        projectDir = pProjectDir;
        gradleVersion = pGradleVersion;
    }



    public void arrangeTestProject(final String pName)
        throws IOException
    {
        final File testDir = projectDir;
        Files.createDirectories(testDir.toPath());
        final File srcDir = new File("src/functionalTest/resources", pName);
        FileUtils.copyDirectory(srcDir, testDir);
    }



    public String act(final boolean pExpectSuccess, final String... pCommandLineArgs)
    {
        final GradleRunner runner = GradleRunner.create();
        runner.forwardOutput();
        runner.withPluginClasspath();
        runner.withProjectDir(projectDir);
        runner.withGradleVersion(gradleVersion);
        runner.withArguments(Stream.concat(
            Arrays.stream(new String[]{"--info", "--stacktrace"}),
            Arrays.stream(pCommandLineArgs)).toArray(String[]::new));
        runner.withDebug(true);

        BuildResult buildResult = pExpectSuccess ? runner.build() : runner.buildAndFail();
        return buildResult.getOutput();
    }



    public void assertAnsiEscape(final String pBuildLog, final boolean pPresenceExpected)
    {
        Matcher matcher = LogXformUtil.ANSI_COLOR_ESCAPES_PATTERN.matcher(pBuildLog);
        Assertions.assertEquals(pPresenceExpected, matcher.find());
    }



    void assertNoWarnings(final String pBuildLog)
    {
        Assertions.assertFalse(pBuildLog.contains("WARNING:"));
    }
}
