In order to execute [:taskA], the following tasks would be executed in this order:

  1. :taskB              (org.gradle.api.DefaultTask)
  2. :taskC              (org.gradle.api.tasks.bundling.Jar)
  3. :included:includedA (org.gradle.execution.plan.TaskInAnotherBuild)
  4. :taskA              (org.gradle.api.DefaultTask)

The above may be missing information on tasks from included builds. The tiTree task may have a little more information. You are seeing this because taskinfo.disableSafeguard=true.
