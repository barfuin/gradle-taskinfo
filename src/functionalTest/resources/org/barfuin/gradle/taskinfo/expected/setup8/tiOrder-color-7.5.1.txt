In order to execute [:taskA], the following tasks would be executed in this order:

[33m  1.[m :taskB              [90m(org.gradle.api.DefaultTask)[m
[33m  2.[m :taskC              [90m(org.gradle.api.tasks.bundling.Jar)[m
[33m  3.[m [90m:included:includedA[m [90m(org.gradle.execution.plan.TaskInAnotherBuild)[m
[33m  4.[m :taskA              [90m(org.gradle.api.DefaultTask)[m

The above may be missing information on tasks from included builds. The tiTree task may have a little more information. You are seeing this because taskinfo.disableSafeguard=true.
